//
// Created by daniel on 8/24/20.
//

#include "../headers/extern.h"

extern int x,y;

void test_function(){
    printf("x has: %d\n",x);
    printf("y has: %d\n",y);
}

void enums(){
    enum boolean {NO,YES};

    printf("No value: %d\n",NO);
    printf("Yes value: %d\n",YES);

    enum months {JAN = 1,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC};

    printf("Jan is: %d\n",JAN);
    printf("Freb is: %d\n",FEB);

    enum vars {BELL2='\a',BELL3};

    printf("Var bell2: %d\n",BELL2);
    printf("Var bell3: %d\n",BELL3);

    char hi2 = 'a';
    printf("%c\n",hi2);
    enum status {SUCCESFULL=1,FAILED=0,STOPED=-1,KILLED='\2',NOT_INIT='2',IDK};

    printf("Successfull: %d\n",SUCCESFULL);
    printf("Failed: %d\n",FAILED);
    printf("Stoped: %d\n",STOPED);
    printf("Killed: %d\n",KILLED);
    printf("Not init: %d\n",NOT_INIT);
    printf("UNKOWN: %d\n",IDK);

    enum status val = STOPED;
    //int i2;
    //puts(i2);
    printf("The value stored in STOPED is %d\n",val);
}

void logics_oper(){
    if ( get_1() < 10 || get_2()>get_3()){
        printf("\nTrue");
    }
    printf("\n%d\n",0b111 | 0b101);


}

int get_1(){
    printf("\nIn get1");
    return 1;
}

int get_2(){
    printf("\nIn get2");
    return 2;
}

int get_3(){
    printf("\nIn get3");
    return 3;
}

void upper_lower_char(){
    char D = 'D';
    char a = 'a';



    printf("D to lower: %c\n",tolower(D));
    printf("a to upper: %c\n",toupper(a));
}

void upper_lower_str(){
    char myname[] = "edgar daniel";

    printf("My lowercase name: %s\n",myname);

    for(int i = 0; myname[i]; i++)
        myname[i] = toupper(myname[i]);

    printf("My uppercase name: %s\n",myname);

    char last_name[] = "MAGALLON villanueva";

    printf("My uppercase lastname: %s\n",last_name);

    for(char *p = last_name; *p; ++p) {
        printf("p has %c\n", *p);
        *p = *p > 0x40 && *p < 0x5b ? *p | 0x60 : *p;
    }

    printf("My lowercaase lastname: %s\n",last_name);

    /*char rev[] = "reverse";

    for(char *p = *rev; rev; --p){
        *p = *p;
    }

    printf("Rev: %s\n",rev);*/


}

