//
// Created by daniel on 8/19/20.
//

#ifndef C_MYLIBRARY_H
#define C_MYLIBRARY_H

#endif //C_MYLIBRARY_H

#define ng_one -1
#define ng_onep (-1)



#include <stdio.h>



int isPair(int num){
    return num%2==0;
}

void read_with_EOF(){
    int c;

    printf("%d\n",ng_one);
    printf("%d\n",ng_onep);

    while((c=getchar()) != EOF)
    {
        putchar(c);
    }
}
