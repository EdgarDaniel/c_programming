//
// Created by daniel on 8/20/20.
//

#include "mylibrary.h"
#include <zconf.h>

#ifndef C_MYCLASS_H
#define C_MYCLASS_H

struct Person{
    char name[30];
    int age;
    int birthday_year;


} ref_person,ref_init={"Edgar Daniel",10,1999l};

struct Animal{
    int weight;
    int age;
};

struct Perro{
    char name[20];
    struct Animal dog;
}mydog,mydog2 = {"Lucas",40,10},layla={"Layla",{25,5}};

struct Laptop{
    char model[15];
    char *p_model;
}toshiba,lap[] = {{"toshiba"},
                   {"lenovo"},{"hp"}};

void dynamic_memory(){
    /**
     * char name[30];
     * If name has "Edgar" it's being used only 5 positions, and the other 25 is being wasted
     */

    int n_caract = 10;
    char * pointer;
    pointer = malloc(n_caract*sizeof(char));

    if (pointer == NULL)
    {
        puts("Error al asignar memoria");
    }else{
        puts("Se asgino memoria");

    }
    puts("Exiting...");
}

void update(){
    puts("-------------------");
    printf("The age is %d\n:",ref_person.age);
    ref_person.age = 89;
    printf("The age is %d\n:",ref_person.age);
    puts("-------------------");
}

void recursive_function(int steps){

    if(steps!=0)
    {
        recursive_function(steps-=1);
        printf("Step: %d\n",steps);
        sleep(1);
    }

}

void pointers(){
    int a = 2;

    int *****pointer = &a;
    int *pointer2 = &a;

    printf("Memory: %p\n",pointer);
    printf("Memory p2: %p\n",pointer2);
    printf("Value: %d\n",*pointer);

}

//Doesnt work
int get_lengthArray(int *array[]){
    return (int) (sizeof(array) / sizeof array[0]);
}

int cubeWithoutRef(int num){
    return num*num*num;
}

void cubeByReference(int* num){
    *num = *num * *num * *num;
}

#endif //C_MYCLASS_H
