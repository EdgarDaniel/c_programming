//
// Created by daniel on 8/31/20.
//

#ifndef C_ORDER_H
#define C_ORDER_H
struct Envase
{
    int id; //Identificador
    int volumen; //Volumen en mililitros
    float precio; //Precio en euros
};

/* Metodo de ordenacion I: ordenar por identificador*/
int ordenarID (const void * a, const void * b)
{
    /*Se hace un casting para convertir los punteros void a punteros de Envases
      y se guarda su identificador*/
    int id_a = ((struct Envase*)a)->id;
    int id_b = ((struct Envase*)b)->id;

    //Se comparan los dos identificadores
    if(id_a > id_b)
        return 1;

    else
        return -1;
}


/* Metodo de ordenacion II: ordenar por precio/unidad de volumen */
int ordenar_precio_volumen (const void * a, const void * b)
{
    /*Se hace un casting para convertir los punteros void a punteros de Envases
      y se guarda su volumen y precio*/
    float precio_a = ((struct Envase*)a)->precio;
    int volumen_a = ((struct Envase*)a)->volumen;
    float precio_b = ((struct Envase*)b)->precio;
    int volumen_b = ((struct Envase*)b)->volumen;

    //Se calcula precio/volumen
    float val_a = precio_a/volumen_a;
    float val_b = precio_b/volumen_b;

    //Se comparan los dos valores
    if(val_a > val_b)
        return 1;

    else
        return -1;
}

#endif //C_ORDER_H
