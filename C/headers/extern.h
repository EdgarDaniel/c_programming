//
// Created by daniel on 8/24/20.
//

#ifndef C_EXTERN_H
#define C_EXTERN_H

#include <stdio.h>
#include <ctype.h>
#define BELL '\007'

void test_function();
void enums();
int get_1();
int get_2();
int get_3();
void logics_oper();
void upper_lower_str();
void upper_lower_char();

#endif //C_EXTERN_H
