//
// Created by daniel on 8/24/20.
//
#include <stdio.h>
#include "./headers/extern.h"
#include <stdbool.h>
#include <zconf.h>
#include <libnet.h>
#include "./headers/order.h"
#include "myclass.h"
#include "./headers/order.h"


//Error:
//int x=0;
int t=-3;

#define MAXLINE 3

int max;
char line[MAXLINE];
char longest[MAXLINE];

int length_of(char s[]);
int get_line(void);
void copy(void);


typedef void (*GeoLayoutCommandProc)(void);

int suma(int a, int b){

    return a+b;
}

int resta(int a,int b){
    return a-b;
}

int operacion(int a, int b,int *funcion(int,int)){

    return funcion(a,b);
}

void swap(int *a, int *b){

    *a = *a ^ *b;
    *b = *b ^ *a;
    *a = *a ^ *b;
}

void function_pointer(int a){
    printf("Inside the funcion pointer %d\n",a);
}

void sockets(){
    struct servent *Puerto;
    Puerto = getservbyname ("Nombre_Servicio", "tcp");
    
}

void typedef_inline(){
    typedef int TipoVectorEnteros [4];
    TipoVectorEnteros numeroDeCoches;
    TipoVectorEnteros numeroDeAviones;

    numeroDeCoches[0] = numeroDeAviones[1] = 32;
    numeroDeCoches[1]=0; numeroDeCoches[2]=0; numeroDeCoches[3]=0;
    printf ("El numero de coches en la hora cero fue %d \n", numeroDeCoches[0]);
    printf ("El numero de coches en la hora uno fue %d \n", numeroDeCoches[1]);
    printf ("El numero de coches en la hora dos fue %d \n", numeroDeCoches[2]);
    printf ("El numero de coches en la hora tres fue %d \n", numeroDeCoches[3]);

    printf ("El numero de aviones en la hora cero fue %d \n", numeroDeAviones[0]);
    printf ("El numero de aviones en la hora uno fue %d \n", numeroDeAviones[1]);
}

void order_envase(){
    int dimension = 4; //Dimension del vector



    struct Envase array[dimension]; //Vector de envases
    array[0].id = 5;
    array[0].volumen = 600;
    array[0].precio = 12;

    array[1].id = 6;
    array[1].volumen = 550;
    array[1].precio = 7.99;

    array[2].id = 1;
    array[2].volumen = 400;
    array[2].precio = 25;

    array[3].id = 3;
    array[3].volumen = 600;
    array[3].precio = 9.5;

    qsort((void*)array, dimension, sizeof(struct Envase), ordenarID);


    //Mostrar el vector por ID. El output es {1 3 5 6}
    printf("\nOrdenado por ID: {");
    for(int i = 0; i < dimension; i++)
    {
        printf("%d ", array[i].id);
    }
    printf("}\n");


    //Ordenar el vector por precio/volumen
    qsort((void*)array, dimension, sizeof(struct Envase), ordenar_precio_volumen);


    //Mostrar el vector por ID. El output ahora es {6 3 5 1}
    printf("\nOrdenado por precio/vol: {");
    for(int i = 0; i < dimension; i++)
    {
        printf("%d ", array[i].id);
    }
    printf("}\n\n");
}

void pointers_structs(){


    struct Person person1, *pointer_person;
    person1.age = 45;
    strcpy(person1.name,"Bryant");
    person1.birthday_year = 20;

    printf("The person1 data is: %s - %d - %d\n",person1.name,person1.age,person1.birthday_year);

    pointer_person = &person1;

    strcpy(&pointer_person->name,"Peter");
    pointer_person->age = 100;

    printf("The person1 data is: %s - %d - %d\n",person1.name,person1.age,person1.birthday_year);

    strcpy(&pointer_person->name,"Linus");
    pointer_person->birthday_year=12;

    printf("The person1 data is: %s - %d - %d\n",person1.name,person1.age,person1.birthday_year);
    printf("The person pointer data is: %s - %d - %d\n",pointer_person->name,
           pointer_person->age,
           pointer_person->birthday_year);
}

void pointers2(){

    int a = 22, b, c[10] = {10,50};
    printf("%d - %d\n",c[0],c[1]);

    int *ip; //apuntador a int

    ip = &a; //ip apunta a "a"
    b = *ip; //b tiene 22

    printf("b, ip and a are: %d - %d - %d\n",b,a,*ip);


    printf("Ip *%d - Ip pointer *%p\n",*ip,*ip);
    printf("Ip %d - Ip pointer %p\n",ip,ip);

    *ip = 0; // a tiene 0

    printf("b, ip and a are: %d - %d - %d\n",b,a,*ip);

    int ind=1;
    ip = &c[ind]; //ip apunya a c[in]

    printf("\n---\nc[ind] = %d - Ip pointer val: *%p - Id pos memo c[ind] and ip: %p - %p\n\n",c[ind],*ip,&c[ind],ip);
    //ip = 100; error
    *ip += 120;
    (*ip)++;
    printf("\n---\nc[ind] = %d - Ip pointer val: *%p - Id pos memo c[ind] and ip: %p - %p\n\n",c[ind],*ip,&c[ind],ip);
    int *iq = ip;
    *iq = 2000;
    printf("\n---\nc[ind] = %d - Ip pointer val: *%p - Id pos memo c[ind] and ip: %p - %p\n\n",c[ind],*ip,&c[ind],ip);
    // *ip = a; //is equal that ip = &a
    //  ip = a; error


}

void vars_register(){
    //later
    register int daniel=21;

}


#define dprint(expr) printf(#expr " = %g\n",expr)

#ifndef YEARS_OLD
    #define YEARS_OLD 12
#endif

//#undef YEARS_OLD

#define paste(front,back) front ## back

#define showList(...) puts(#__VA_ARGS__)

double average(int count,...){

    va_list valist;
    double sum = 0.0;
    int i;

    /* initialize valist for num number of arguments */
    va_start(valist,count);

    /* access all the arguments assigned to valist */
    for(i=0; i<count; i++)
    {
        sum += va_arg(valist,double);
    }
    printf("Sum: %f\n",sum);

    //clear memory
    va_end(valist);

    return sum/count;
}

#define FUNCTION(name, a) int fun_##name() { printf("On method: %s\n",__FUNCTION__ ); return a;}
FUNCTION(abcd, 12)
FUNCTION(efg, 122)

void strings(){

    char s1[] = "Hello";
    char s2[] = " World";
    char s3[] = "World";

    strcat(s1,s2);
    printf("S: %s\n",s1);
    printf("Comparing %s withn %s = %d\n",s2,s3,strcmp(s2,s3));
}

void print_structs(int count,...){

    va_list valist;
    int i;

    /* initialize valist for num number of arguments */
    va_start(valist,count);
    struct Person ref;
    /* access all the arguments assigned to valist */
    for(i=0; i<count; i++)
    {

        ref = va_arg(valist,struct Person);

        printf("Person name: %s\n",ref.name);
        printf("Person age: %d\n",ref.age);
        printf("Person birthday: %d\n",ref.birthday_year);

        puts("\n-----------\n");
    }

    //clear memory
    va_end(valist);

}

struct Person get_struct(){
    struct Person p;
    strcpy(p.name,"Juan");
    p.age = 100;
    p.birthday_year = 2000;

    return p;
}

void pointer_struct(struct Person* ref){
    strcpy(ref->name,"Ref");
    ref->age = 10;
    ref->birthday_year = 1000;
}

union DataBase{
    int id;
    char name[30];
}reg1;

/* define simple structure */
struct {
    unsigned int widthValidated;
    unsigned int heightValidated;
} status1;

/* define a structure with bit fields */
typedef struct {
    unsigned int widthValidated : 1;
    unsigned int heightValidated : 1;
} status2;

#if !defined(A)
#endif


enum DataType{
    TString,
    TInt,
    TDouble,
    TFloat,
    TLong
};


void method_generic(int size,void* data[],enum DataType obj){

    int i;

    switch (obj) {
        case TString:
        {
            char *pointer;
            for(pointer = (char *) data; *pointer; ++pointer)
            //for(i=0; i<size; i++)
            {
                printf("The value is %c\n",*pointer);
                //printf("The %d value is %s\n",i+1,data[i]);
            }

            printf("The value is %c\n",*pointer);
            printf("The value is %s\n",(char *)data[0]);
        }
        break;

        case TInt:
        {
            for(int *pointer = (int *) data; *pointer; ++pointer)
            {
                printf("the value is: %d\n",*pointer);
            }
        }
        break;

        case TDouble:
            break;

        case TFloat:
            break;

        case TLong:
            break;
    }
}

void print_array_strings(){

    int i = 1, *ip = &i;

    char *sports[] = {
            "golf",
            "hockey",
            "football",
            "cricket",
            "shooting"
    };

    char **pointer = &sports[0];
    sports[1] = "swimming";
    printf("The value is: %s\n",*pointer);
    *pointer = "baseball";
    printf("The value is: %s\n",sports[0]);

    for(i = 0; i < 5; i++)
    {
        printf("String = %10s", sports[i] );
        printf("\tAddress of string literal = %u\n", sports[i]);
    }

    //Now lets see same array without using pointer
    char name2[3][20] = {
            "Adam",
            "chris",
            "Deniel"
    };

    //Wrong: name2[0]="Edgar";
    strcpy(name2[0],"Edgar");
    printf("First value: %s\n",name2[0]);
}

void matrices_punteros(){
    int *p[2]; //array de puntero
    int a[2][2] = {{10,20},{100,200}};
    printf("a[0]: %d - %d\n",a[0][0],a[0][1]);
    int i;
    p[0] = a[0];
    printf("p[0]: %d - %d\n",p[0][0],p[0][1]);
    //p[0] =2 {3,4};
    printf("p[0]: %d - %d\n",p[0][0],p[0][1]);
    printf("a[0]: %d - %d\n",a[0][0],a[0][1]);
    //for (i=0;i<5;i++){
        //puntero[i]=&a[i]; //Asignamos las filas al puntero.
        //printf("Puntero[%d] = %d\n",i,*puntero[i]);
    //}
}

int main() {

    matrices_punteros();

   /* int matriz[3][3] = {{10,20,30},
                        {40,50,60},
                        {70,80,90}};

    printf("Matriz[1][4] = %d\n",matriz[1][4]);
    printf("Matriz[2][1] = %d\n",matriz[2][1]);

    int *pointer = matriz;//&matriz[0][0];
    *pointer = *(pointer+(1*3)+2); //value of postion [1][2]
    printf("*pointer has: %d\n",*pointer);

    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++)
        {
            printf("%d - ",matriz[i][j]);
        }
        puts("\n");
    }*/


    /*int data[] = {10,40,21};
    int *p = data;
    printf("The 1st value is: %d\n",*p);
    printf("The 2nd value is: %d\n",*(p+1));
    method_generic(3,data,TInt);

    print_array_strings();*/

    //char dataString[3][10] = {"Peter", "Robert", "Chris"};
    //printf("Value: %s\n",dataString[0]);
    //method_generic(3,dataString,TString);
    /**
     * printf("Value: %s",dataString[0]);
     * char dataString[3][10] = {{"Peter"}, {"Robert"},{ "Chris"}};
     *
     * it will print PRC (the first letter of each row/register)
     */
    //method_generic(3,&dataString,TString);

    //printf("Reg1 before: %d\n",reg1.id);
    //Print after modified, because get corrupted
    /*printf( "Memory size occupied by status1 : %d\n", sizeof(status1));
    printf( "Memory size occupied by status2 : %d\n", sizeof(status2));
    strcpy(reg1.name,"Daniel");
    printf("Reg1: %s\n",reg1.name);
    reg1.id=10;
    printf("Reg1: %d\n",reg1.id);


    struct Person p1,p2;
    p1 = get_struct();
    pointer_struct(&p2);*/
    /*strcpy(p1.name,"Daniel");
    p1.age = 10;
    p1.birthday_year = 1999;
    strcpy(p2.name,"Edgar");
    p2.age = 21;
    p2.birthday_year = 1997;*/

    //print_structs(2,p1,p2);
    //print_structs(1,p1);

    //strings();

    //FUNCTION(get,25);

    /*printf("Average: %f\n",average(5,10.0,10.0,10.0,10.0,10.0));

    printf("func abcd: %d\n",fun_abcd());
    printf("func efg: %d\n",fun_efg());

    printf("func: %s\n",__func__);
    printf("Pretty Function: %s\n",__PRETTY_FUNCTION__ );
    printf("File %s\n",__FILE__);
    printf("B. File %s\n",__BASE_FILE__);
    printf("Line %d\n",__LINE__);
    printf("Date %s\n",__DATE__);
    printf("Time %s\n",__TIME__);
*/
    //#ifdef,#ifndef

    #ifdef __linux__
        printf("My so is GNU/Linux\n");
    #endif

    #ifdef __unix__
        printf("My so is based unix\n");
    #endif

    #ifdef _WIN64
        printf("My so is bugindows\n");
    #endif

    #if defined(YEARS_OLD)
        printf("TechOnTheNet is over %d years old.\n", YEARS_OLD);
    #endif

    printf("TechOnTheNet is a great resource.\n");

    showList(Daniel,is,21,years,old);

    int paste(n,1);
    n1 = 100;
    printf("the n1 is: %d\n",n1);

   // dprint(10.0f/3.0f);
    //pointers_structs();
    //typedef_inline();
    //order_envase();
/*    int a = 100,b = 50;

    printf("The sum of %d and %d is %d\n",a,b,operacion(a,b,&suma));
    printf("The difference of %d and %d is %d\n",a,b,operacion(a,b,&resta));*/

    /*void (*p_function)(int) = &function_pointer;
    p_function(100);*/

  /*  int a=100,b=200;
    printf("a and b before swap: %d - %d\n",a,b);
    swap(&a,&b);
    printf("a and b after swap: %d - %d\n",a,b);*/

    //pointers2();

    //long t =1000ul;  -> unsigned long
    //float f = 122.33l;
    //long double d = 2.2;
    //double long s = 23.3;
    //x = 0;
    //test_function();

    //char c[] = {'D','a','\0','\1'};

    //printf("%s\n",c);
    //110001 - 49; 110000 - 48
    //printf("%c%c%c%c",c[0],c[1],c[2],c[3]);

    /*bool val = 'a'<'b';
    printf("%d-%d : %d%d\n",'1','0','10');
    int num = '2'-'1';
    printf("%d\n",num);
    printf("%d\n",val);
    printf("%c\n",BELL);
    char name[6] = {'E','d','g','a','r','\0'};
    char name0[] = {'E','d','g','a','r'};
    char name3[] = "Edgar";

    int dd[] = {1,2,3,4,5};
    int dd2[] = {2,4,22};

    for(int i=0; i<5; i++){
        printf("%d - ",dd[i]);
    }
    printf("\n");

    for(int i=0; i<3; i++){
        printf("%d - ",dd2[i]);
    }

    printf("\nthe length of %s is %d\n",name,length_of(name));
    printf("\nthe length of %s is %d\n",name0,length_of(name0));
    printf("\nthe length of %s is %d\n",name3,length_of(name3));

    enums();
    logics_oper();
    upper_lower_char();
    upper_lower_str();
    */

    return 0;
}



int length_of(char string[])
{
   int i = 0;

   while(string[i]!='\0')
       ++i;

   return i;
}

void doesnt_work(){
    int len;
    extern int max;
    extern char longest[];

    max = 0;

    while ((len=get_line())>0) {
        printf("Len: %d\nmax: %d\n", len, max);
        if (len > max) {
            max = len;
            copy();
        }
    }

    if(max > 0)
        printf("%s - ",longest);

}

int get_line(){
    int c,i;
    extern char line[];

    for(i=0; i < MAXLINE - 1 && (c=getchar()) != EOF && c != '\n'; ++i)
        line[i] = c;

    if(c == '\n'){
        line[i] = c,
        ++i;
    }


    line[i] = '\0';
    return i;
}

void copy(){
    int i;

    extern char line[],longest[];

    i=0;

    while ((longest[i]=line[i]) != '\0')
        i++;
}