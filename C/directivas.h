//
// Created by daniel on 8/20/20.
//


#ifndef C_DIRECTIVAS_H
#define C_DIRECTIVAS_H
#define PI 3.14159 //symbolic constant
#define Cube(a) a*a*a //macro

#define FOR_PRINT(a,b) for(;a<=b; a++){ printf("%d\n",a);
#define FOR(A,B) for(; A < B ; A++)
#define maximo(a,b) (a > b) ? a : b


#endif //C_DIRECTIVAS_H
