#include <protobuf-c/protobuf-c.h>

struct person {
  char *name;
  int age;
};

static const ProtobufCMessageDescriptor person_descriptor = {
  .name = "person",
  .fields = (ProtobufCFieldDescriptor[]) {
    {
      .name = "name",
      .type = PROTOBUF_C_TYPE_STRING,
      .label = PROTOBUF_C_LABEL_REQUIRED,
    },
    {
      .name = "age",
      .type = PROTOBUF_C_TYPE_INT32,
      .label = PROTOBUF_C_LABEL_REQUIRED,
    },
    {
      .name = NULL,
      .type = 0,
      .label = PROTOBUF_C_LABEL_REPEATED,
    },
  },
};

void serialize_person(struct person *person, ProtobufCBuffer *buffer) {
  protobuf_c_message_pack(person, buffer);
}

struct person *deserialize_person(ProtobufCBuffer *buffer) {
  struct person *person = protobuf_c_message_unpack(person_descriptor.base, buffer);
  return person;
}

int main() {
  struct person person = {"John Doe", 30};
  ProtobufCBuffer *buffer = protobuf_c_message_new_buffer(person_descriptor.base);
  serialize_person(&person, buffer);
  struct person *deserialized_person = deserialize_person(buffer);
  printf("%s is %d years old\n", deserialized_person->name, deserialized_person->age);
  protobuf_c_message_free_buffer(buffer);
  return 0;
}

