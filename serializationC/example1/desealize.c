#include <stdio.h>
#include <stdlib.h>
#include "person.pb-c.h"

int main() {
  FILE *fp = fopen("person.bin", "rb");
  fseek(fp, 0L, SEEK_END);
  size_t size = ftell(fp);
  fseek(fp, 0L, SEEK_SET);

  uint8_t buffer[size];
  fread(buffer, 1, size, fp);

  Person *person = person__unpack(NULL, size, buffer);
  printf("Name: %s\n", person->name);
  printf("Age: %d\n", person->age);

  person__free_unpacked(person, NULL);
  fclose(fp);
  return 0;
}
