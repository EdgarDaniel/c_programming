#include <stdio.h>
#include "person.pb-c.h"

int main() {
  Person person = PERSON__INIT;
  person.name = "John";
  person.age = 30;

  size_t size = person__get_packed_size(&person);
  uint8_t buffer[size];
  person__pack(&person, buffer);

  // send the buffer over the network or store it in a file
  fwrite(buffer, 1, size, stdout);
  return 0;
}
