#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>

// noreturn should not be used on void return functions
noreturn void func(void) {
  puts("This functions should never return!");
  exit(0);
}

int main(void) {

  // func(10,20); // deprecated ofr func() (without void specifier)

  puts("UNDEFINED_BEHAVIOUR examples:\n");
  //@UNDEFINED_BEHAVIOUR
  puts("int i = 42;\n"
       "printf( %d %d, i++, i);\n");

  puts("Modifying a const variable using a pointer:\n"
       "const int foo_readonly = 10;\n"
       "int *foo_ptr;\n"
       "foo_ptr = (int *)&foo_readonly; /* (1) This casts away the const\n"
       "qualifier */"
       "*foo_ptr = 20;                  /* This is undefined behavior */\n");

  // This behaves differently in gcc and clang
  // const int foo_readonly = 10;
  // printf("%d\n",foo_readonly);
  // int *foo_ptr;
  // foo_ptr = (int *)&foo_readonly; /* (1) This casts away the const qualifier
  // */ *foo_ptr = 20;                  /* This is undefined behavior */
  // printf("%d\n",foo_readonly);
  //

  puts("Incosisten linkage of identifiers:\n"
       "extern int var;\n"
       "static int var; /* Undefined behaviour */\n"
       "but:\n"
       "\n/* 1. This is NOT undefined */\n"
       "static int var;\n"
       "extern int var;\n"
       "\n/* 2. This is NOT undefined */\n"
       "static int var;\n"
       "static int var;\n");

  puts("Returning on 'noreturn' functions:\n"
       "It does not matter the type of the function (can be void or any other "
       "data type):\n"
       "Calling func() [noreturn func()]");
  func();
  puts("This text will not be printed");
  return 0;
}
