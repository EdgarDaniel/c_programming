#include <stdio.h>
#include <stdlib.h>

int main(void) {
  struct date {
    // d has value between 0 and 31, so 5 bits
    // are sufficient
    unsigned int d : 5;

    // m has value between 0 and 15, so 4 bits
    // are sufficient
    unsigned int m : 4;

    int y;
  };

  // https://www.geeksforgeeks.org/bit-fields-c/
  printf("Size of date is %lu bytes\n", sizeof(struct date));
  struct date dt = {31, 12, 2014};
  printf("Date is %d/%d/%d\n", dt.d, dt.m, dt.y);
}
