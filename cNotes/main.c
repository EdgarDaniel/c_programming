#include <stdio.h>
#include <stdlib.h>
#define QUOTE(...) #__VA_ARGS__ // Variadic function

int main(void) {

  puts("hello World\n");

  puts("Comments can be: //someone or /*something*/\n");

  puts("Comments with trigraph, use: ??/ or what is equivalent: \\\n");

  puts("The '??/' trigraph is actually a longhand notation for \\, which is "
       "the line continuation symbol. This means that the compiler thinks the "
       "next line is a continuation of the current line, that is, a "
       "continuation of the comment, which may not be what is intended\n");

  puts(QUOTE(This line uses QUOTE(...) hack \n));

  

  // EXIT_SUCCESS is defined in stdlib
  return EXIT_SUCCESS;
}
