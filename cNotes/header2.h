#include "stdio.h"
#pragma once

#ifndef HEADER_2_H

#define HEADER_2_H

void fnFromHeade2(void);
inline void fnFromHeade2(void) { puts("Header 2"); };

#endif // !HEADER_2_H

void fnFromHeade2_2(void);
inline void fnFromHeade2_2(void) { puts("Header 2.2"); }
