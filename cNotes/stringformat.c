#include <stdio.h>
#include <custom/colors.h>
#include <string.h>

void fprinting(){
  puts("\n");
  char buffer[50];
  double PI = 3.141592;
  sprintf(buffer,"PI value is: %fl", PI);

  yellow();
  printf("buffer variable has: %s [len: %zu]\n",buffer,strlen(buffer));
  reset();
}

void fsread(){
  puts("\n");
  char sentence[] = "date : 06-06-2012";
  char str[50];

  int year,month,day;
  darkcyan();
  sscanf(sentence, "%s : %2d-%2d-%4d",str,&day,&month,&year);
  printf("%s -> %02d-%02d-%4d\n",str,day,month,year);
  reset();
}

int main()
{
  fprinting();
  fsread();
}
