#include <stdio.h>

void green();
void red();
void blue();
void yellow();
void reset();

inline void green() { puts("\033[0;32m"); }

inline void red() { puts("\033[0;31;9m"); }

inline void blue() { puts("\033[0;34m"); }

inline void yellow() { puts("\033[0;33m"); }

inline void reset() { printf("\033[0m"); }
