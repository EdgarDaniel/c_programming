#include <custom/colors.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {

  FILE *fout;
  int last_error = 0;
  if ((fout = fopen(argv[1], "r")) == NULL) {
    last_error = errno;
    /* reset errno and continue */
    errno = 0;
  }
  /* do some processing and try opening the file differently, then */

  if (last_error) {
    fsetRGBFg(stderr, 255, 107, 90);
    fprintf(stderr, "fopen: Could not open file '%s' for reading: %s\n",
            argv[1], strerror(last_error));
    fputs("Cross fingers and continue\n", stderr);
    freset(stderr);
  }

  return errno;
}
