#include <string.h>
#include <stdio.h>
#include <custom/colors.h>

int main(void)
{
  grey();
  printf("Char: %s (lent: %zu)\n","\u204D",strlen("\u204D"));
  puts("emoticon: \u2020 and \342\200\240");
  puts("emoticon: \046");
  puts("emoticon: \u069B");
  puts("\a");
  reset();
}
