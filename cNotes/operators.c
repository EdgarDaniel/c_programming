#include "colors.h"
#include <stdalign.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void comma() {

  yellow();
  puts("\n====================Using comma ',' "
       "operator==============================\n");
  int x = 30, y = 10;
  printf("(x*=3) = %i\n", (x *= 3));
  printf("(y*=3,x) = %i\n", (y *= 3, x));
  printf("y = %i\n", y);
  printf("(4,1,45,10,0,100,34) = %i", (4, 1, 45, 10, 0, 100, 34));
  int k;
  for (k = 1; k < 10; printf("k has %d\n", k), k += 2)
    ;
  puts("\n====================        END             "
       "==============================\n");
  reset();
}

void accessop() {

  yellow();
  puts("\n====================Using access '.' and '->' "
       "operator==============================\n");

  struct MyStructType {
    int x;
    int y;
  };

  struct MyStructType myObj, myObj2;

  myObj.x = 100;
  myObj.y = 200;

  myObj2 = myObj;

  blue();
  printf("myObj.x = %i and myObj.y = %i\n", myObj.x, myObj.y);
  printf("myObj2.x = %i and myObj2.y = %i\n", myObj2.x, myObj2.y);
  struct MyStructType *myObjPointer;
  myObjPointer = &myObj;

  printf("myObjPointer>x = %i and myObjPointer>y = %i\n", myObjPointer->x,
         myObjPointer->y);
  myObjPointer->x = 1;
  myObjPointer->y = 2;

  red();
  printf("myObj.x = %i and myObj.y = %i\n", myObj.x, myObj.y);
  printf("myObj2.x = %i and myObj2.y = %i\n", myObj2.x, myObj2.y);
  printf("myObjPointer>x = %i and myObjPointer>y = %i\n", myObjPointer->x,
         myObjPointer->y);

  green();
  puts("\n====================        END             "
       "==============================\n");
}

void addressof() {

  blue();
  puts("\n====================Using adress of '&'"
       "operator==============================\n");

  int y1 = 120;
  int *py1 = &y1;
  int valpy1 = *py1;
  int *address = py1;
  printf("%p = %p\n", (void *)&y1, (void *)py1);
  printf("y1 = %d and py1 = %d\n", y1, *py1);
  printf("valpy1 has %d\n", valpy1);
  printf("address has %p with value %d\n", address, *address);
  reset();
}

void indexing() {
  blue();
  // printf("\e[1;1H\e[2J");
  puts("\n====================Indexing==============================\n");
  yellow();
  int array[] = {10, 45, 22, 55, 90};
  printf("array[%d] = %d\n", 2, array[2]);
  printf("*(array + %d) = %d\n", 1, *(array + 1));
  blue();
  puts("\n====================END==============================\n");
  reset();
}

void sizeofop() {
  blue();
  puts("\n====================sizeof()==============================\n");
  green();
  unsigned long sizeul = sizeof(int);
  size_t size_st = sizeof(long);
  // int x9 = size_st;
  // printf("%d\n",x9);

  // From man printf (3):
  //  z      A following integer conversion corresponds to a size_t or ssize_t
  //  argument,
  //            or a following n conversion corresponds to a pointer to a size_t
  //            argument.
  puts("Types as operand:");
  printf("\tsizeof(int) = %zu\n", sizeul);
  printf("\tsizeof(long) = %zu\n", size_st);

  puts("\nExpressions as operand");
  char car = 'C';
  long longv = 1L;
  char *lit = "Yés!";
  printf("\tcar='C' ; sizeof(car) = %zu\n", sizeof(car));
  printf("\tlongv=1L ; sizeof longv = %zu\n", sizeof longv);
  printf("\tlit=\"Yés!\" ; sizeof lit = %lu\n", strlen(lit));
  blue();
  puts("\n====================END==============================\n");
  reset();
}

void casting() {
  int x = 10;
  int y = 3;

  printf("x/y = %d\n", x / y);
  // Here the value of x is converted to a double, the division promotes the
  // value of y to double, too, and the result of the division, a double is
  // passed to printf for printing.
  printf("(double) x/y = %.3f\n", (double)x / y);
}

int funccall(int x, int y) {
  //  printf("The product is: %d\n",x*y);
  return x * y;
}

void assigment() {
  float reals[] = {10.23, 23.34, 45.53, 11.12, 34.0};
  float *pos = reals;
  // int x =10;
  // int * address = &x;
  // printf("adress of x = %p,%p\n",&x,address);
  printf("p has: %p - so reals[0] = %f\n", pos, *pos);
  pos += 1;
  printf("p has: %p - so reals[next] = %f\n", pos, *pos);
  pos += 1;
  printf("p has: %p - so reals[next] = %f\n", pos, *pos);
  printf("*(reals + %d) = %f\n", 1, *(reals + 1));
}

void alignment() {
  // max_align_t
  yellow();
  printf("\nAligment of char = %zu\n", alignof(char));
  printf("Aligment of max_align_t = %zu\n", alignof(max_align_t));
  printf("Aligment of float[10] = %zu\n", alignof(float[10]));
  printf("Aligment of float[5] = %zu\n", alignof(float[5]));
  printf("Aligment of char * = %zu\n", alignof(char *));
  printf("Aligment of struct{char c ; int n;} = %zu\n", alignof(struct {
           char c;
           int n;
         }));
  printf("Aligment of struct{char * c ; int n;} = %zu\n", alignof(struct {
           char *c;
           int n;
         }));
  typedef struct {
    char *c;
    double n;
    long unsigned int x;
  } S1;
  struct {
    struct S1 *obj;
    S1 obj2;
    char *c;
    double n;
    long unsigned int x;
    size_t y;
  } S2;
  printf(
      "Aligment of struct{char * c ; double n; long unsigned int x;} = %zu\n",
      alignof(S1));
  printf("Aligment of struct{struct S1 *obj; char * c ; double n; long "
         "unsigned int x;size_t y;} = %zu\n",
         alignof(S2));

  typedef struct A {
    char c;
    float f;
    short s;
  } A;

   typedef struct B {
    float f;
    short s;
    char c;
  } B;

  printf("size of A: %zu\n", sizeof(A)); // size of A: 12;
  printf("size of B: %zu\n", sizeof(B)); // size of B: 8;
  reset();
  puts("");
}

int main(void) {

  // typedef void (*fns) (void);
  // fns funcs[] = {&assigment};
  //  This causes a segm. fault:
  //  int sizearr=979923576;
  //  int data[sizearr];
  //  This does not:
  //  int *data = malloc(sizearr * sizeof *data);
  int opc, nitems, ch;
  char var = 'C';
  printf("%c\n", var);
  var--;
  printf("%c\n", var);

  while (1) {
    puts("1) Indexing");
    puts("2) Access Op.");
    puts("3) sizeof()");
    puts("4) Casting");
    puts("5) funccall");
    puts("6) assigment");
    puts("7) alignment");
    puts("0) Exit");
    printf("Choose an option> ");
    nitems = scanf(" %d", &opc);

    if (nitems == EOF) {
      red();
      puts("Failed to read stdin");
      reset();
      break;
    } else if (nitems == 0) {
      getchar();
      red();
      puts("Not items matched");
      reset();
      while ((ch = getchar()) != '\n' && ch != EOF)
        ;
    } else {
      switch (opc) {
      case 1:
        indexing();
        break;
      case 2:
        accessop();
        break;
      case 3:
        sizeofop();
        break;
      case 4:
        casting();
        break;
      case 5:
        int (*fn)(int, int) = &funccall;
        int a = 25, b = 4;
        green();
        printf("\n(*fn)(%d,%d) = %d\n", a, b, (*fn)(a, b));
        printf("fn(%d,%d) = %d\n\n", 12, 5, fn(12, 5));
        reset();
        break;
      case 6:
        assigment();
        break;
      case 7:
        alignment();
        break;
      case 0:
        return 0;
      }
    }
    // if (opc == EOF) {
    //   /* ... you're not going to get any input ... */
    //   puts("contining...");
    //   continue;
    // }
  }
  puts("bye!");
}
