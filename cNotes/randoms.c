#include <custom/colors.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// This code is PCG32 from pcg-random.org, a modern, fast, general-purpose RNG
// with excellent statistical properties. It's not cryptographically secure, so
// don't use it for cryptography.
typedef struct {
  uint64_t state;
  uint64_t inc;
} pcg32_random_t;
uint32_t pcg32_random_r(pcg32_random_t *rng) {
  uint64_t oldstate = rng->state;
  /* Advance internal state */
  rng->state = oldstate * 6364136223846793005ULL + (rng->inc | 1);
  /* Calculate output function (XSH RR), uses old state for max ILP */
  uint32_t xorshifted = ((oldstate >> 18u) ^ oldstate) >> 27u;
  uint32_t rot = oldstate >> 59u;
  return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
}
void pcg32_srandom_r(pcg32_random_t *rng, uint64_t initstate,
                     uint64_t initseq) {
  rng->state = 0U;
  rng->inc = (initseq << 1u) | 1u;
  pcg32_random_r(rng);
  rng->state += initstate;
  pcg32_random_r(rng);
}

int main(void) {
  int i;
  // printf("time(NULL) is %ld\n",time(NULL));
  srand(time(NULL));
  i = rand();
  printf("Random Value between [0, %d]: %d\n", RAND_MAX, i);

  orange();
  puts("\nThis method of generating randoms (by using rand()) should not be "
       "used for serious RNG needs, like crypto");
  puts("Instead can use the library: https://github.com/jedisct1/libsodium");
  reset();

  cyan();
  puts("\nFor LINUX:");
  puts("Use random() and srandom()");

  srandom(time(NULL));
  long i2 = random();
  printf("Random Value between [0, %ld]: %ld\n", LONG_MAX, i2);

  pcg32_random_t rng; /* RNG state */

  /* Seed the RNG */
  pcg32_srandom_r(&rng, 42u, 54u);
  /* Print some random 32-bit integers */
  for (i = 0; i < 6; i++)
    printf("0x%08x\n", pcg32_random_r(&rng));

  yellow();
  puts("\nSee about xorshift  method");
  return 0;
}
