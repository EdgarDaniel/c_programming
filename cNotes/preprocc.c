#include "header1.h"
#include "header2.h"
#include <math.h>
#include <stdio.h>

#pragma warn - rvl /* return value */
#pragma warn - par /* parameter never used */
#pragma warn - rch /*unreachable code */

#ifndef _MATH_H
#error First include math header files
#else
void printSum(void);
#endif /* ifndef __MATH_H */

#pragma GCC poison reads

// void reads3(void) {
//   struct R {
//     int x;
//     int b;
//   } reads;
// }
//

#if 1
// use if 0 for 'commenting'
  void paramnoused(int x) { puts("In x"); }
#endif

#define concat(x,y) x ## y

// void fnFromHeade1(void){
//   puts("I'm in header 1");
// }
//
// void fnFromHeade2(void){
//   puts("I'm in header 2");
// }
//

void predefinedMacros(){

  // __func__ is a array, not a macro
  printf("__FILE__ is: %s\n",__FILE__);
  printf("__LINE__ is: %d\n",__LINE__);
  printf("__DATE__ is: %s\n",__DATE__);
  printf("__TIME__ is: %s\n",__TIME__);
  printf("__func__ is: %s\n",__func__);
  printf("__STDC_VERSION__ (yyyymm, type long) is: %ld\n",__STDC_VERSION__);
}

#define debug_print(msg, ...) printf(msg, __VA_ARGS__);

void variadicArgs(int status){
  if(status == 0){
    debug_print("[line: %d, file: %s] %s",__LINE__,__FILE__, "Error, status is equal to 0.\n");
  }else{
    debug_print("[OK] %s","Good!\n");
  }
}

#define SQUARE(x) x*x
int main(void) {
  fnFromHeade1();
  fnFromHeade2();
  fnFromHeade2_2();
  paramnoused(33);
  char * AB = "Bye,adios";
  char * str = concat(A,B);
  puts(str);
  predefinedMacros();
  variadicArgs(0);
  variadicArgs(1);
  return 0;
}
