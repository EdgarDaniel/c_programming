#include "colors.h"
#include <inttypes.h>
#include <locale.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <uchar.h>
#include <wchar.h>

// #define __STDC_WANT_LIB_EXT1__ 1
// #include <string.h>
// #ifndef __STDC_LIB_EXT1__
// # error "we need strtok_s from Annex K"
// #endif

void p1() {

  char *str = "Hello, world";
  printf("char *str = \"%s\": This a string literal\n", str);

  char a1[] = "abc";
  printf("char a1[] = \"%s\": a1 is char[4] holding {'a','b','c','\\0'}\n", a1);

  char a2[4] = "abc";
  printf("char a2[] = \"%s\": same as a2\n", a2);

  char a3[3] = "abc";
  printf("char a3[] = \"%s\": a3 is char[3] holding {'a','b','c'}, missing the "
         "'\\0'\n",
         a3);
  puts("Previously, the a3 output was duplicated: 'abcabc', that's because %s "
       "expects a null terminated string ('\\0')");

  puts("\nStrings literals (such as char *str ...) are not modifiable");
  puts("It's recommended to make them as const");
  char const *s = "foobar";
  printf("s has: %s\n", s);
  // s[0] = 'B';
  // printf("s now has: %s\n",s);

  puts("\nChar arrays can be modified:");
  a1[0] = 'Z';
  a1[2] = 'o';
  printf("a1 now has : %s\n", a1);

  puts("Multiple string literals:");
  char *ms = "Linux - "
             "MacOS - "
             "Alpine Linux - "
             "RedHat - "
             "SUSE";
  printf("ms has %s\n", ms);

  puts("\nUsing % PRId8 (PRId8 is a macro from <inttypes.h>)");
  char *fmt = "%" PRId8;
  printf("fmt has %s\n", fmt);

  puts("\nString literals with special chars:");
  char *special1 = "¿äó×🌙";
  char *chinesse = "世界, 你好!";
  printf("special1 has %s\n", special1);
  printf("chinesse has %s\n", chinesse);

  // unexpected behaviour: (it will print also the contetns of another variable)
  // run with: -fsanitize=undefined,address
  // char special2[1] = "🌙";
  // printf("special2 has %s\n", special2);
  setlocale(LC_ALL, "");
  puts("\nString literals support different character sets (use "
       "setlocale(LC_ALL,\"\"))");
  wchar_t *s2 = L"ä²©";
  printf("wchar_t *s2 has %ls\n", s2);

  puts("\nUTF-8 strings:");
  char *s3 = u8"áb© 世界, 你好!";
  printf("(UTF-8 string literal, of type char[]) uf8 s3 = %s\n", s3);

  char16_t *s4 = u"🍌";
  // char32_t c = U'🍌';
  //  printf("(16-bit wide string literal, of type char16_t[]) char16_t s4 =
  //  %s\n",s4);
  // puts(s4);
  // wprintf(L"Banana emoji: %zu\n",s4);
  puts("byee");
}

void alterCharArray(char array[]) {
  array[0] = 'W';
  array[1] = 'a';
}

void tokenisation() {

  blue();
  int toknum = 0;
  char src[] = "Hello,, world!";
  printf("src is %s\n", src);
  const char delimiters[] = ", !";
  // looks like strtok should be avoided
  char *token = strtok(src, delimiters);

  while (token != NULL) {
    printf("%d: [%s]\n", ++toknum, token);
    token = strtok(NULL, delimiters);
  }
  // strtok does not allocate new memory for the tokens, it modifies the source
  // string
  printf("Source has now: %s\n", src);
  reset();
  puts("");
}

void non_reentrant_strtok() {
  /*
   * The expected operation is that the outer do while loop should create three
tokens consisting of each decimal number string ("1.2", "3.5", "4.2"), for each
of which the strtok calls for the inner loop should split it into separate digit
strings ("1", "2", "3", "5", "4", "2"). However, because strtok is not
re-entrant, this does not occur. Instead the first strtok correctly creates the
"1.2\0" token, and the inner loop correctly creates the tokens "1" and "2". But
then the strtok in the outer loop is at the end of the string used by the inner
loop, and returns NULL immediately. The second and third substrings of the src
array are not analyzed at all.
   */

  char src[] = "1.2,3.5,4.2";
  char *first = strtok(src, ",");
  do {
    char *part;
    /* Nested calls to strtok do not work as desired */
    printf("[%s]\n", first);
    part = strtok(first, ".");
    while (part != NULL) {
      printf(" [%s]\n", part);
      part = strtok(NULL, ".");
    }
  } while ((first = strtok(NULL, ",")) != NULL);
}

void reentrant_strtok() {
  char src[] = "1.2,3.5,4.2";
  char *next = NULL;
  char *first = strtok_r(src, ",", &next);

  do {
    char *part;
    char *posn;
    printf("[%s]\n", first);
    part = strtok_r(first, ".", &posn);
    while (part != NULL) {
      printf(" [%s]\n", part);
      part = strtok_r(NULL, ".", &posn);
    }
  } while ((first = strtok_r(NULL, ",", &next)) != NULL);
}

void literals() {
  yellow();
  puts("A char * pointer cannot be modified per position/index");
  puts("So using:\n\tchar * str = \"Hello\"; str[0] = 'W' is an undefined "
       "behaviour");
  char *str1 = "Hi";
  printf("str1 = %s\n", str1);
  puts("Here str1 is a pointer to the first element of str1, which is 'H'");
  // here the program may crash
  // str1[0] = 'L'; printf("str1 = %s\n", str1);
  puts("Instead, the full variable (or pointer) may change what points to by "
       "assinging another literal");
  puts("Now, str1 = \"World!\"");
  str1 = "Wórld!";
  printf("str1 = %s\n", str1);
  size_t len = strlen(str1);
  printf("strlen(str1) = %zu\n", len);

  // use str2[4] of length 4, since 'Hi!' contains at the end the character \0
  char str2[4] = "Hi!";
  printf("str2 = %s\n", str2);
  size_t len2 = strlen(str2);
  printf("strlen(str2) = %zu\n", len2);

  char *lit2 = "Yeo is god";
  printf("'Yes![1]' = %c\n", "Yes!"[1]);
  printf("lit2[0]=%c\n", lit2[0]);
  // lit2 points to address of 'Y' char or &char[0]
  char *plit2 = lit2;

  while (*plit2 != '\0') {
    printf("plit2 in next index: %c\n", *(plit2++));
  }

  puts("");
  reset();
}

void ascii_utf8() {
  blue();
  puts("Hello World in Greek");

  char asciiString[50] = "Hello world!";
  char utf8String[50] = "Γειά σου Κόσμε!";

  printf("asciiString has %zu bytes in the array\n", sizeof(asciiString));
  printf("utf8String has %zu bytes in the array\n", sizeof(utf8String));
  printf("asciiString is %zu bytes\n", strlen(asciiString));
  printf("utf8String is %zu bytes\n", strlen(utf8String));

  puts("");
  reset();
}

void copying() {
  int a = 10, b, *g;
  g = &a;
  *g = 22;
  green();
  char c[] = "abc", *d, e[4];
  static char f[5]; // if the variable is declared as global or static, it is
                    // initialized with \0

  // From ChatGPT:
  // When you assign the address of x to y, y now points to the first element of
  // the array x. However, y is a separate variable with its own memory
  // location, and is distinct from the memory location of x itself
  printf("a = %d\n", a);
  printf("b = %d\n", b);
  printf("c = %s\n", c);
  printf("d = %s\n", d);
  printf("g = %d and points to a: both a[%p],g[%p] have different address\n",
         *g, &a, (void *)&g);
  printf("\tbut a[index] and g[index] points to same address: (a[0],g[0]) = "
         "(%p,%p)\n",
         &a, &g[0]);
  // printf("d = %s\n", d);
  // printf("e = %s\n", e);
  printf("static f = %s\n\n", f);

  blue();

  puts("Making b = a and d = c");
  b = a;

  /* Only copies the address of the string -
  there is still only one string stored in memory */
  d = c;
  printf("a = %d\n", a);
  printf("b = %d\n", b);
  printf("c = %s\n", c);
  printf("d = %s\n", d);
  printf("Both c y d points to same address location: c[%p],d[%p]\n", &c[0],
         &d[0]);

  d[0] = 'A';
  c[1] = 'Z';
  puts("using d[0]='A' and c[1]='Z'");
  printf("c = %s\n", c);
  printf("d = %s\n", d);

  char h[] = "hij";
  // Compile error
  // char j[] = h;
  // char j[3] = h;
  getchar();
  yellow();
  puts("Using strcopy");

  char i[8];

  printf("h has %s\n", h);
  printf("i has %s\n", i);
  puts("Making strcpy...");
  strcpy(i, h);
  printf("h has %s\n", h);
  printf("i has %s\n", i);
  i[0] = '9';
  puts("using i[0]='9'");
  printf("h has %s\n", h);
  printf("i has %s\n", i);

  getchar();
  char k[5];
  puts("Using char k[2]; strcpy(k,i) will cause and undefined behaviour so use "
       "better: strncpy() or snprintf()");
#if 0
    strcpy(k, i);  /* causes buffer overrun (undefined behavior), so do not execute this here! */
  //printf("k has %s\n", k);
  //k[0] = 'A';
  //printf("k has %s\n", k);
#endif
  // differences: https://stackoverflow.com/a/55132336/13594705
  snprintf(k, sizeof(k), "=%s", i);
  // strncpy(k, i, size);
  printf("k has %s\n", k);
  k[0] = 'A';
  printf("k has %s\n", k);

  getchar();
  green();

  puts("Using strncat()");
  char dest[32];
  char *source2 = "This is a string.";

  // The strncat() function appends the first count characters of string2 to
  // string1 and ends the resulting string with a null character (\0). If count
  // is greater than the length of string2, the length of string2 is used in
  // place of count.
  //
  // The strncat() function operates on null-ended strings. The string argument
  // to the function should contain a null character (\0) marking the end of the
  // string.
  dest[0] = 'N';
  dest[1] = 'o';
  dest[2] = '\0';
  strncat(dest, source2, sizeof(dest) - 1);
  printf("dest has %s\n", dest);

  blue();
  puts("Using strncat safely");
  char dst[24] = "Clownfish: ";
  char src[] = "Marvin and Nemo";
  size_t len = strlen(dst);

  printf("sizeof(dst)=%zu and len=%zu\n", sizeof(dst), len);
  printf("Using strncat(dst, src, sizeof(dst) - len -1) will concat the first "
         "%zu bytes from %s into %s\n",
         sizeof(dst) - len - 1, src, dst);
  strncat(dst, src, sizeof(dst) - len - 1);
  printf("%zu: [%s]\n", strlen(dst), dst);

  char *xy = "Hi";
  printf("%c and %c\n\tis xy[2]=='\\0'? %s\n", xy[0], xy[2],
         xy[2] == '\0' ? "true" : "false");

  puts("");
  reset();
}

void iterateOverStrings() {
  blue();

  puts("Using string literals:");
  char *str = "OpenSuse Tumbleweed";
  printf("Str is %s with length %zu\n", str, strlen(str));
  size_t i = 0;

  for (; i < strlen(str); i++) {
    printf("str[%zu] = %c\n", i, str[i]);
  }

  yellow();
  char str2[9] = "OpenSuse";

  printf("\nStr2 is %s with length %zu\n", str2, strlen(str2));
  i = 0;

  while (str2[i] != '\0') {
    printf("str2[%zu] = %c\n", i, str2[i]);
    i++;
  }

  // for(; i<strlen(str2); i++){
  //   printf("str[%zu] = %c\n",i,str[i]);
  // }

  puts("");
  reset();
}

void arrayOfStrings() {
  yellow();
  
  // when we assign string literals to char *, the strings themselves are
  // allocated in read-only memory.
  // However, the array string_array is allocated in read/write memory. This
  // means that we can modify the pointers in the array, but we cannot modify
  // the strings they point to.
  char *names[] = {"Edgar Daniel Magallon Villanueva", "Luis Jose Martinez",
                   "Pedro Sola", ""};

  printf("sizeof names[0] = %zu\n", sizeof(names[0]));
  printf("names[] array has a size of: %zu\n",
         sizeof(names) / sizeof(names[0]));

  char **np = names;
  while (strcmp(*np, "") != 0) {
    printf("*np has: %s and pointer [%p,%p]\n", *np, np, &np);
    *np++;
  }

  char modifiable_string_array_literals[][4] = {"foo", "bar", "baz"};
  char equivalent_up_string_array[][4] = {
      {'f', 'o', 'o', '\0'}, {'b', 'a', 'r', '\0'}, {'b', 'a', 'z', '\0'}};
  // np+=strlen(names[0])+1;
  //  printf("*np has: %s and pointer %p\n",np,&np);

  puts("");
  reset();
}


void parseStringToNumbers() {
  blue();
  char str[30] = "2030300 This is test";
  char *ptr;
  long ret;

  printf("String to parse: %s\n", str);
  ret = strtol(str, &ptr, 10);
  printf("The number(unsigned long integer) is %ld\n", ret);
  printf("String part is |%s|\n", ptr);
  // For double use: strtod()
  puts("");
  reset();
}

void FormattingIO(void){
  
}

int main(int argc, char *argv[]) {
  // int main() {

  // printf("Sizeof argv: %zu\n",sizeof(*argv));
  int opc, nitems, ch;
  char *nerdchar = "ℛ";
  // nerdchar[0] = 'P';
  printf("%s is of length: %zu\n", nerdchar, strlen(nerdchar));
  printf("%%c[0] %%c[1] %%c[2] = [%d] [%d] [%d]\n", nerdchar[0], nerdchar[1],
         nerdchar[2]);
  while (1) {

    puts("1) wchar,utf,...");
    puts("2) Tokenisation");
    puts("3) Char array");
    puts("4) Non re-entrant strtok()");
    puts("5) Re-entrant strtok()");
    puts("6) Literals");
    puts("7) ascii_utf8");
    puts("8) Copying");
    puts("9) String iteration");
    puts("10) Array of strings");
    puts("11) Parse String");
    puts("12) Formatting I/O");
    puts("0) Exit");
    printf("Choose an option> ");
    nitems = scanf(" %d", &opc);

    if (nitems == EOF) {
      red();
      puts("Failed to read stdin");
      reset();
      break;
    } else if (nitems == 0) {
      getchar();
      red();
      puts("Not items matched");
      reset();
      while ((ch = getchar()) != '\n' && ch != EOF)
        ;
    } else {
      switch (opc) {
      case 1:
        p1();
        break;
      case 2:
        tokenisation();
        break;
      case 3: {
        char myarr[] = "Nell xD";
        green();
        printf("myarr has: %s\n", myarr);
        alterCharArray(myarr);
        printf("myarr has: %s\n", myarr);
        puts("");
        reset();
        break;
      }
      case 4:
        non_reentrant_strtok();
        break;
      case 5:
        reentrant_strtok();
        break;
      case 6:
        literals();
        break;
      case 7:
        ascii_utf8();
        break;
      case 8:
        copying();
        break;
      case 9:
        iterateOverStrings();
        break;
      case 10:
        arrayOfStrings();
        break;
      case 11:
        parseStringToNumbers();
        break;
      case 12:
        FormattingIO();
        break;
      case 0:
        puts("Byee!");
        return 0;
      }
    }
  }

  // Pointers
  // int a=10, *p;
  // p = &a;
  // printf("p has %d\n",*p);
  // // *p+=1;
  // ++(*p);
  // printf("p has %d\n",*p);
  // // *p+=1;
  // ++(*p);
  // printf("p has %d\n",*p);
  // int ints[] = {11,12,13,14};
  // int * pint = ints;
  // printf("ints[] points to %p\n",ints);
  // printf("ints[0] points to %p\n",&ints[0]);
  // printf("pint points to %p\n",pint);
  //
  // pint = &ints[1];
  // printf("pint points to %p\n",pint);

  int q = 102, *r, *t;
  r = &q;
  // *&q and *r prints 102
  printf("*(q and r,&r): (%p,%p,%p)\n", &q, r, &r);
  printf("q and r: (%d,%d)\n", q, *r);
  *r = -1;
  printf("q and r: (%d,%d)\n", q, *r);
  t = r;
  // t = &*r;
  printf("q - r - t: (%d | %d | %d)\n", q, *r, *t);
  printf("q - r - t: (%p | %p | %p)\n", &q, r, t);
  *t = 839;
  printf("q - r - t: (%d | %d | %d)\n", q, *r, *t);

  unsigned int u[] = {1, 2, 12, 45, 10, 0xFF};
  unsigned int *r2 = &u[0];
  int count = 0;
  while (count != 5) {
    printf("[%d] = %u\n", count++, *r2);
    r2++;
  }
  r2 -= 4;
  printf("%u\n", *r2);
  printf("%u\n", *(r2 + 2));
  printf("%u\n", *r2);

  int z[5];
  *z = 10;
  *(z + 1) = 20;
  *(z + 2) = 30;
  *(z + 3) = 40;
  // *(z+4) = 45;
  // int * z4 = (z+4);

  printf("sizeof(z)/sizeof(z[0]) = %zu\n", sizeof(z) / sizeof(z[0]));

  for (size_t i = 0; i < sizeof(z) / sizeof(z[0]); i++) {
    printf("z[%zu] = %d\n", i, z[i]);
  }
  // printf("z[4] = %d\n",*z4);

  // This is valid:
  // char ms[9];
  // printf("Size of ms: %zu\n",sizeof(ms));
  puts("Byee!");
  return 0;
}
