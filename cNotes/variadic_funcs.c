#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

int variadic_fns(int n,...){
 
  va_list ptrlist;
  int sum=0;

  va_start(ptrlist,n);

  for (int i=0; i<n; i++) {
    int * ptr = (int *)(&n + ((i+1)*4));
    printf("Current num: %d\n",*ptr);
    sum+=va_arg(ptrlist,int);
  }

  va_end(ptrlist);

  int * np = &n;
  *np = 890;
  return sum;
}

int main(void)
{
  int n=5;
  printf("Sum: %d\n",variadic_fns(n,1,100,400,9,11));
  return EXIT_SUCCESS;
}
