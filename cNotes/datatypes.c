#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int main(void) {

  printf("%s%31s\n", "Expression", "Interpretation");
  printf("%s%43s\n", "thing[X]", "an array of size X of...");
  printf("%s%55s\n", "thing(t1, t2, t3)",
         "a function taking t1, t2, t3 and returning...");
  printf("%s%36s\n", "*thing", "a pointer to...");

  puts("\nExamples");

  puts("\nchar *names[20]:");
  puts("\t[] takes precedence over *, so the interpretation is: names is an "
       "array of size 20 of a pointer to char.");

  puts("\nchar (* place)[10]:");
  puts("\tIn case of using parentheses to override the precedence, the * is "
       "applied first: place is a pointer to an array of size 10 of char");

  puts("\nint *fn(void);:");
  puts("The () is applied first: fn is a function taking void and returning a "
       "pointer to int.");

  puts("\nint (*fp)(void);:");
  puts("Overriding the precedence of (): fp is a pointer to a function taking "
       "void and returning int.");

  puts("\nint **ptr;:");
  puts("\tThe two dereference operators have equal precedence, so the "
       "associativity takes effect. The operators are applied in right-to-left "
       "order: ptr is a pointer to a pointer to an int.");

  puts("\nMultiple declarations:");
  puts("\nint fn(void), *ptr, (*fp)(int), arr[10][20], num;");
  puts("\tfn: a function taking void and returning int;");
  puts("\tptr: a pointer to an int;");
  puts("\tfp: a pointer to a function taking int and returning int;");
  puts("\tarr: an array of size 10 of an array of size 20 of int;");
  puts("\tnum: int. ");

  puts("\n============================================================\n");
  puts("\nIntegers:");
  uint32_t u32 = 32;
  uint8_t u8 = 255;
  int64_t i64 = -65;
  printf("\tuint32_t u32 = %d: %s\n ", u32, "exactly 32-bits wide");
  printf("\tuint8_t u8 = %d: %s\n ", u8, "exactly 8-bits wide");
  printf("\tint64_t i64 = %ld: %s\n ", i64,
         "exactly 64-bits in two's complement representation");
  puts("\n============================================================\n");

  puts("\n============================================================\n");
  puts("\nInteger types and constants:");
  signed char c = 65;
  signed short int si = 32767;
  signed int i = 32767;
  signed long int li = 2147483647;
  signed long long int lli = 2147483647;
  printf("\tsigned char c = 65; == %c: %s\n ", c, "required to be 1 byte");
  printf("\tsigned short int si = %d: %s\n ", si,
         "required to be at least 16 bits");
  printf("\tsigned int i = %d: %s\n ", i, "required to be at least 16 bits");
  printf("\tsigned long int li = %ld: %s\n ", li,
         "required to be at least 32 bits");
  printf("\tsigned long long int lli = %lld: %s (Use ########LL to get a long "
         "long typed literal.))\n ",
         lli, "required to be at least 64 bits");
  puts("\nOn major 32-bit platforms:"
       "\n\tint is 32 bits"
       "\n\tlong is 32 bits as well"
       "\n\tlong long is 64 bits");
  puts("\nOn major 64-bit platforms:"
       "\n\tint is 32 bits"
       "\n\tlong is either 32 or 64 bits"
       "\n\tlong long is 64 bits as well");

  puts("\nOr using : #include <stdint.h>"
       "\n\tint8_t and uint8_t"
       "\n\tint16_t and uint16_t"
       "\n\tint32_t and uint32_t"
       "\n\tint64_t and uint64_t");

  /* suffixes to describe width and signedness : */
  long int vi = 0x32;        /* no suffix represent int, or long int */
  unsigned int vui = 65535u; /* u or U represent unsigned int, or long int */
  long int vli = 65536l;     /* l or L represent long int */

  puts("\n============================================================\n");

  exit(0);
}
