#include <custom/colors.h>
#include <stdio.h>
#include <stdlib.h>

void print_all(FILE *stream) {
  char c;

  cyan();
  while ((c = getc(stream)) != EOF) {
    putchar(c);
  }
  reset();
}

void runProcess() {
  FILE *stream;

  char *cmd = "ls --color=yes /home/edgar/";
  if ((stream = popen(cmd, "r")) == NULL)
    return;

  print_all(stream);
  pclose(stream);
}

void getLinesFile() {
  yellow();
  puts("\nReading file:\n");
  darkcyan();
  char *line_buf = NULL;
  size_t line_buf_size = 0;
  int line_count = 0;
  ssize_t line_size;
  const char *filename = "/home/edgar/mbox";
  FILE *fp = fopen(filename, "r");

  if (!fp) {
    fsetRGBFg(stderr, 255, 107, 90);
    fprintf(stderr, "Error opening file '%s'\n", filename);
    perror(filename);
    freset(stderr);
    return;
  }

  line_size = getline(&line_buf, &line_buf_size, fp);

  while (line_size >= 0) {
    line_count++;

    printf("line[%06d]: chars=%06zd, buf size=%06zu, contents: %s", line_count,
           line_size, line_buf_size, line_buf);

    line_size = getline(&line_buf, &line_buf_size, fp);
  }

  free(line_buf);
  line_buf = NULL;

  fclose(fp);

  reset();
  puts("\n");
}

void filescan(){

  puts("\nScanning words of file:");

  FILE *fp;

  if( (fp = fopen("/home/edgar//chamba", "r")) == NULL ){
    perror("Error opening file /home/edgar/chamba");
    exit(1);
  }

  char tmp[20];
  int i=1;

  while( (fscanf(fp, "%19s", tmp)) != EOF ){
    printf("Word %d: %s\n",i++,tmp);
  }

  fclose(fp);
  // this should not be used:
  // free(fp);
}

int main(int argc, char *argv[]) {

  char *path = (argc > 1) ? argv[1] : "outputfile.txt";

  FILE *file = fopen(path, "w");

  if (!file) {
    perror(path);
    return EXIT_FAILURE;
  }

  if (fputs("Output in file.\n", file) == EOF) {
    perror(path);
    return EXIT_FAILURE;
  }

  if (fclose(file)) {
    perror(path);
    return EXIT_FAILURE;
  }

  runProcess();
  getLinesFile();
  filescan();
}
