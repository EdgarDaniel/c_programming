#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#define boolean int
#define ttrue 1
#define ffalse 0

int main(void)
{

  bool x = true;
  bool y = false;

  if(x){
    puts("Sucess on x");
  }else{
    puts("Non-success on x");
  }
  
  if(!y){
    puts("Non-Sucess on y");
  }else{
    puts("Success on y");
  }

  printf("is [x == 1]? R: (%s) because value of x is %i\n",x==1 ? "true" : "false",x);
  printf("is [y == 0]? R: (%s) because value of y is %i\n",y==1 ? "true" : "false",y);

  boolean y2 = ffalse;
  boolean x2 = ttrue;

  printf("Value of (x2,y2): (%i,%i)\n",x2,y2);
  printf("[printf hack] Using same argument twice: arg(%1$s,%1$s)\n","pedro");


  return EXIT_SUCCESS;
}
