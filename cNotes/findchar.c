#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <custom/colors.h>

int main(int argc, char *argv[]) {
  printf("Filename: %s\n\n",argv[0]);
  char toSearchFor = 'A';

  if (argc != 2) {

    fsetRGBFg(stderr,246, 104, 89);
    fprintf(stderr,"[argc: %d] Argument missing. Expected one\n", argc);
    return EXIT_FAILURE;
  }

  yellow();

  printf("Search the character %c in string %s\n",toSearchFor,argv[1]);

  char *firstOcc = strchr(argv[1], toSearchFor);

  orange();
  if(firstOcc!=NULL){
    printf("First position of %c in %s is %td [%s]\n",toSearchFor,argv[1],firstOcc-argv[1],firstOcc);
  }else{
    printf("%c is not in %s\n",toSearchFor,argv[1]);
  }

  char *lastOcc = strrchr(argv[1], toSearchFor);
  if(lastOcc!=NULL){
    printf("Last position of %c in %s is %td [%s]\n",toSearchFor,argv[1],lastOcc-argv[1],lastOcc);
  }else{
    printf("%c is not in %s\n",toSearchFor,argv[1]);
  }

  return EXIT_SUCCESS;
}
