	.file	"pointers.c"
	.text
	.section	.rodata
	.align 8
.LC0:
	.string	"[char] Void Pointer value: %c\n"
.LC1:
	.string	"[int] Void Pointer value: %d\n"
	.align 8
.LC2:
	.string	"[string/char *] Void Pointer value: %s\n"
	.align 8
.LC3:
	.string	"[double] Void Pointer value: %fl\n"
	.text
	.globl	printPointerValue
	.type	printPointerValue, @function
printPointerValue:
.LFB6:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -40(%rbp)
	movl	%esi, -44(%rbp)
	cmpl	$3, -44(%rbp)
	je	.L2
	cmpl	$3, -44(%rbp)
	ja	.L7
	cmpl	$2, -44(%rbp)
	je	.L4
	cmpl	$2, -44(%rbp)
	ja	.L7
	cmpl	$0, -44(%rbp)
	je	.L5
	cmpl	$1, -44(%rbp)
	je	.L6
	jmp	.L7
.L5:
	movq	-40(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -21(%rbp)
	movl	$0, %eax
	call	yellow
	movsbl	-21(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %eax
	call	reset
	jmp	.L3
.L6:
	movq	-40(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -20(%rbp)
	movl	$0, %eax
	call	orange
	movl	-20(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %eax
	call	reset
	jmp	.L3
.L4:
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	movl	$0, %eax
	call	darkcyan
	movq	-16(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %eax
	call	reset
	jmp	.L3
.L2:
	movq	-40(%rbp), %rax
	movsd	(%rax), %xmm0
	movsd	%xmm0, -8(%rbp)
	movl	$0, %eax
	call	green
	movq	-8(%rbp), %rax
	movq	%rax, %xmm0
	movl	$.LC3, %edi
	movl	$1, %eax
	call	printf
	movl	$0, %eax
	call	reset
	nop
.L3:
.L7:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
	.size	printPointerValue, .-printPointerValue
	.section	.rodata
.LC5:
	.string	"Hi, there."
.LC6:
	.string	"\n\n"
	.text
	.globl	voidPointers
	.type	voidPointers, @function
voidPointers:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	$10, -12(%rbp)
	movsd	.LC4(%rip), %xmm0
	movsd	%xmm0, -24(%rbp)
	leaq	-12(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	$1, %esi
	movq	%rax, %rdi
	call	printPointerValue
	leaq	-24(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	$3, %esi
	movq	%rax, %rdi
	call	printPointerValue
	movb	$90, -25(%rbp)
	leaq	-25(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	printPointerValue
	movq	$.LC5, -40(%rbp)
	leaq	-40(%rbp), %rax
	movl	$2, %esi
	movq	%rax, %rdi
	call	printPointerValue
	movl	$.LC6, %edi
	call	puts
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	voidPointers, .-voidPointers
	.globl	fun
	.type	fun, @function
fun:
.LFB8:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$10, -4(%rbp)
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8:
	.size	fun, .-fun
	.section	.rodata
	.align 8
.LC7:
	.string	"Wildpointers are uninitialized pointers\nThese pointers usually point to some arbitrary memory location and may cause a program to crash or misbehave\nFor example, using int *p; *p=10 may crash or will missbehave"
	.text
	.globl	wildpoiters
	.type	wildpoiters, @function
wildpoiters:
.LFB9:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$.LC7, %edi
	call	puts
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9:
	.size	wildpoiters, .-wildpoiters
	.section	.rodata
.LC8:
	.string	"Memory could not be allocated"
.LC9:
	.string	"Memory allocated"
	.text
	.globl	nullpointer
	.type	nullpointer, @function
nullpointer:
.LFB10:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$200, %edx
	movl	$144, %esi
	movl	$69, %edi
	call	setRGBFg
	movq	$0, -8(%rbp)
	movl	$8, %edi
	call	malloc
	movq	%rax, -8(%rbp)
	cmpq	$0, -8(%rbp)
	jne	.L13
	movl	$.LC8, %edi
	call	puts
	jmp	.L14
.L13:
	movl	$.LC9, %edi
	call	puts
.L14:
	movl	$0, %eax
	call	reset
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	free
	movq	$0, -8(%rbp)
	movl	$.LC6, %edi
	call	puts
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10:
	.size	nullpointer, .-nullpointer
	.globl	main
	.type	main, @function
main:
.LFB11:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$0, %eax
	call	voidPointers
	movl	$0, %eax
	call	nullpointer
	movl	$0, %eax
	call	wildpoiters
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11:
	.size	main, .-main
	.section	.rodata
	.align 8
.LC4:
	.long	1791379320
	.long	1074872307
	.ident	"GCC: (SUSE Linux) 13.0.1 20230421 (prerelease) [revision f980561c60b0446cc427595198d7f3f4f90e0924]"
	.section	.note.GNU-stack,"",@progbits
