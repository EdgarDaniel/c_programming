#include <custom/colors.h>
#include <stdlib.h>

typedef enum { CHAR, INT, STRING, DOUBLE } DATA_TYPE;

void printPointerValue(void *data, DATA_TYPE type) {

  switch (type) {
  case CHAR: {
    char value = *(char *)data;
    yellow();
    printf("[char] Void Pointer value: %c\n", value);
    reset();
  } break;

  case INT: {
    int value = *(int *)data;
    orange();
    printf("[int] Void Pointer value: %d\n", value);
    reset();
  } break;

  case STRING: {
    char *value = *(char **)data;
    darkcyan();
    printf("[string/char *] Void Pointer value: %s\n", value);
    reset();
  } break;

  case DOUBLE: {
    double value = *(double *)data;
    green();
    printf("[double] Void Pointer value: %fl\n", value);
    reset();
  } break;
  }
}

void voidPointers() {
  int n = 10;
  double m = 4.312452;
  void *ptr = &n;
  printPointerValue(ptr, INT);
  ptr = &m;
  printPointerValue(ptr, DOUBLE);
  char charac = 'Z';
  printPointerValue(&charac, CHAR);
  char *string = "Hi, there.";
  printPointerValue(&string, STRING);
  puts("\n\n");
}

// dangling pointer:
int *fun() {
  int num = 10;
  return &num;
}

void wildpoiters() {
  puts("Wildpointers are uninitialized pointers\n"
       "These pointers usually point to some arbitrary memory location and may "
       "cause a program to crash or misbehave\n" "For example, using int *p; *p=10 may crash or will missbehave");
}

void nullpointer() {
  setRGBFg(69, 144, 200);
  int *ptr = NULL;
  ptr = malloc(2 * sizeof(int));

  if (ptr == NULL) {
    puts("Memory could not be allocated");
  } else {
    puts("Memory allocated");
  }
  reset();

  // A dangling poiinter is a pointer which points to some non-existing memory
  // location, so after using free(pointer) it should be assgined to NULL
  // segmentation fault:
  // ptr = fun();
  // printf("fun returned: %d\n",*ptr);
  free(ptr);
  // this is undefined behavior, dont do it
  ptr = NULL;
  // int x=122;
  // ptr = &x;
  // printf("ptr has: %d\n",*ptr);
  puts("\n\n");
}

int main(void) {
  voidPointers();
  nullpointer();
  wildpoiters();
  return 0;
}
