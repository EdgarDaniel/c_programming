#include <string.h>
#include <custom/colors.h>
#include <stdio.h>
#include <stdlib.h>

void readIntArray(int *ptr, unsigned int size) {
  for (int i = 0; i < size; i++) {
    printf("Enter an integer: ");
    scanf(" %d", &ptr[i]);
    // scanf(" %d", ptr+i);
  }
}

void showIntArray(int *ptr, unsigned int size) {

  for (int i = 0; i < size; i++) {
    printf("Typed in [%d] = %d\n", i, *(ptr + i));
  }
}

void mallocInts() {
  puts("\n");
  int n;

  orange();
  puts("Memory allocated by malloc is initialized to some garbage value");
  reset();

  printf("Enter the number of integers: ");
  scanf("%d", &n);

  // int *ptr = (int *) malloc(n*sizeof(int));
  int *ptr = malloc(n * sizeof(int));
  if (ptr == NULL) {
    printf("Memory not available\n");
    exit(1);
  }
  yellow();
  showIntArray(ptr, n);
  readIntArray(ptr, n);
  showIntArray(ptr, n);
  free(ptr);
  ptr = NULL;
  reset();
  puts("\n");
}

void callocInts() {
  puts("\n");
  int n;

  orange();
  puts("Memory allocated by calloc is initialized to 0");
  reset();

  printf("Enter the number of integers: ");
  scanf("%d", &n);
  int *ptr = calloc(n, sizeof(int));
  if (ptr == NULL) {
    printf("Memory not available\n");
    exit(1);
  }
  showIntArray(ptr, n);
  readIntArray(ptr, n);
  showIntArray(ptr, n);

  free(ptr);
  ptr = NULL;
}

void reallocInts() {
  cyan();
  puts("\nRealloc function is used to change the size of the memory block "
       "without losing the old data");

  int n;
  reset();
  printf("Enter the number of integers: ");
  scanf("%d", &n);
  int *ptr = malloc(n * sizeof(int));
  if (ptr == NULL) {
    printf("Memory not available\n");
    exit(1);
  }
  setRGBFg(71, 165, 229);
  printf("[before realloc] Address of ptr: %p\n", ptr);

  readIntArray(ptr, n);
  showIntArray(ptr, n);

  printf("Enter the new number of integers: ");
  scanf("%d", &n);
  // it's a good practice realloc to temp variable
  int *tmp = realloc(ptr, n * sizeof(int));
  if (ptr == NULL) {
    printf("Memory not available\n");
    exit(1);
  }
  ptr = tmp;
  printf("[after realloc] Address of ptr: %p\n", ptr);

  showIntArray(ptr, n);
  readIntArray(ptr, n);
  showIntArray(ptr, n);

  free(ptr);
  ptr = NULL;
}

// this is valid, is not a dangling pointer:
int *fun() {
  int *num = malloc(5 * sizeof num);
  return num;
}

struct Laptop {
  char *model;
  char *OS;
};

void printLaptopData(struct Laptop laptop){
  printf("Laptop model: %s [len: %zu, sizeof: %zu]\n",laptop.model,strlen(laptop.model),sizeof(laptop.model));
  printf("Laptop OS: %s\n",laptop.OS);
}

void allocateStruct() {

  setRGBFg(203, 145, 83);

  struct Laptop *mac, *hp;

  mac = malloc(sizeof *mac);
  hp = malloc(sizeof *hp);

  // char * model = malloc(256);
  // printf("type the model: ");
  // scanf("%s",model);

  mac->OS = "MacOs Monterrey";
  mac->model = "Mac M1";
  // mac->model = malloc(256);
  // strcpy(mac->model, model);
  // mac->model = model;
  printLaptopData(*mac);
  mac->model = "Mac M2";
  printLaptopData(*mac);

  hp->model = "HP";
  hp->OS = "Linux, OpenSuse";
  printLaptopData(*hp); 

  struct Laptop laps[2] = {
    {"Lenovo","Ubuntu"},
    {"SteamDeck","SteamOS"}
  };

  printLaptopData(laps[0]);
  printLaptopData(laps[1]);

  free(mac);
  free(hp);
  mac = hp = NULL;

  reset();
}

int main(void) {
  allocateStruct();
  mallocInts();
  callocInts();
  reallocInts();
  return 0;
}
