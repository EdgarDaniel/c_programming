#include <custom/colors.h>
#include <stdio.h>

void registerExa() {

  orange();

  puts("\n\nHW_register example:");

  typedef union {
    struct {
      unsigned char byte1;
      unsigned char byte2;
      unsigned char byte3;
      unsigned char byte4;
    } bytes;
    unsigned int dword;
  } HW_Register;

  HW_Register reg;

  setRGBFg(180, 142, 173);
  reg.dword = 0x12345678;
  printf("dword: %1$u (%1$x)\n", reg.dword);
  printf("\tbyte1: %c\n", reg.bytes.byte1);
  printf("\tbyte2: %c\n", reg.bytes.byte2);
  printf("\tbyte3: %c\n", reg.bytes.byte3);
  printf("\tbyte4: %c\n", reg.bytes.byte4);
  reg.bytes.byte1 = 'Y';
  reg.bytes.byte2 = 'E';
  reg.bytes.byte3 = 'O';
  reg.bytes.byte4 = 'X';
  printf("dword: %1$u (%1$x)\n", reg.dword);
  printf("\tbyte1: %1$c (hex code: %1$x)\n", reg.bytes.byte1);
  printf("\tbyte2: %1$c (hex code: %1$x)\n", reg.bytes.byte2);
  printf("\tbyte3: %1$c (hex code: %1$x)\n", reg.bytes.byte3);
  printf("\tbyte4: %1$c (hex code: %1$x)\n", reg.bytes.byte4);

  puts("\nUsing reg.bytes.byte4=0x5a (is Z):");
  reg.bytes.byte4 = 0x5a;
  printf("dword: %1$u (%1$x)\n", reg.dword);
  printf("\tbyte1: %1$c (hex code: %1$x)\n", reg.bytes.byte1);
  printf("\tbyte2: %1$c (hex code: %1$x)\n", reg.bytes.byte2);
  printf("\tbyte3: %1$c (hex code: %1$x)\n", reg.bytes.byte3);
  printf("\tbyte4: %1$c (hex code: %1$x)\n", reg.bytes.byte4);
  reset();
}

void unions() {
  typedef union abc {
    int a;
    char b;
  } ABCUnion;

  ABCUnion union1;

  printf("\nsizeof(ABCUnion) or sizeof(union1)= %zu,%zu\n", sizeof(ABCUnion),
         sizeof(union1));
  green();
  union1.a = 65;
  printf("\tunion.a = %d\n", union1.a);
  printf("\tunion.b = %c\n", union1.b);
  union1.b = 'c';
  printf("\tunion.a = %d\n", union1.a);
  printf("\tunion.b = %c\n", union1.b);
  reset();
  // union1.b = 'Z';
}

void shop() {

  setRGBFg(125, 72, 73);
  puts("\n\nCreating store object");

  // #pragma pack(1)
  struct store {
    double price;

    union {

      struct {
        char *title;
        char *author;
        int num_pages;
      } book;

      struct {
        int color;
        int size;
        char *design;
      } shirt;
    } item;
  };

  setRGBFg(72, 166, 230);
  struct store book, shirt;
  puts("\n\tBook:");
  book.item.book.title = "Linux System Programming";
  book.item.book.author = "Richard";
  book.item.book.num_pages = 579;
  book.price = 500;
  printf("\tTitle: %s\n", book.item.book.title);
  printf("\tAuthor: %s\n", book.item.book.author);
  printf("\tPages: %d", book.item.book.num_pages);
  printf("\tPrice: %lf\n", book.price);
  printf("\tSizeof (struct) book: %zu\n", sizeof(book));

  setRGBFg(255, 107, 90);
  puts("\n\tShirt:");
  shirt.item.shirt.design = "Nike";
  shirt.item.shirt.size = 24;
  shirt.item.shirt.color = 0xFFAA;
  shirt.price = 1020;
  printf("\tDesign: %s\n", shirt.item.shirt.design);
  printf("\tSize: %d\n", shirt.item.shirt.size);
  printf("\tColor Hex: %x", shirt.item.shirt.color);
  printf("\tPrice: %lf\n", shirt.price);
  printf("\tSizeof (struct) shirt: %zu\n", sizeof(shirt));
  puts("\n");
  reset();
}

typedef union {
  int integer;
  char character;
  double decimal;
  char *string;
} Object;

void object() {
  setRGBFg(254, 209, 30);
  int n = 5;
  puts("\nArray of objects (int,double,char,char *):");
  char types[] = {'I', 'C', 'D', 'S', 'S'};
  Object data[n];
  data[0].integer = 10;
  data[1].character = 'L';
  data[2].decimal = 3.1415;
  data[3].string = "Linux Suse";
  data[4].string = "Linux RedHat";

  puts("Objects of array are:");

  for (size_t i = 0; i < n; i++) {
    // if (__builtin_types_compatible_p(typeof(data[i]), double)) {
    //   puts("Is dobule");
    // }
    switch (types[i]) {
    case 'I':
      printf("\tObject[%zu]: %d\n", i, data[i].integer);
      break;
    case 'C':
      printf("\tObject[%zu]: %c\n", i, data[i].character);
      break;
    case 'D':
      printf("\tObject[%zu]: %lf\n", i, data[i].decimal);
      break;
    case 'S':
      printf("\tObject[%zu]: %s\n", i, data[i].string);
      break;
    }
  }
}

int main(void) {
  printf("%zu\n", sizeof(long));
  setRGBFg(246, 116, 0);
  puts("Union is a user defined data type but unlike structures, union members "
       "share same memory location.\nIf we make changes in one member then it "
       "will be reflected to other members as well.\nSize of the union is "
       "taken according to the size of the largets member");
  reset();
  unions();
  registerExa();
  shop();
  object();
  return 0;
}
