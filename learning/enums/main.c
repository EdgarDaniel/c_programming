#include <custom/colors.h>

typedef enum  {False=1,True,Maybe} Bool;
typedef enum  {Double} Decimal;
// Not allowed:
// typedef enum  {Double} Floating;


int main(void)
{
  setRGBFg(0,100,242);
  puts("Enums");
  Bool var;
  printf("\nUsing enum Bool = {False,True,Maybe} (sizeof: %zu)\n",sizeof(Bool));
  white();
  var = False;
  printf("\tValue of var = False: %d\n",var);
  var = True;
  printf("\tValue of var = True: %d\n",var);
  var = Maybe;
  printf("\tValue of var = Maybe: %d\n",var);

  Decimal dec;
  dec = Double;
  printf("\tValue of var = True: %d\n",dec);
  reset();
  return 0;
}
