#include <stdio.h>

extern void green();
extern void red();
extern void blue();
extern void yellow();
extern void pink();
extern void gray();
extern void darkgray();
extern void orange();
extern void darkblue();
extern void cyan();
extern void lightcyan();
extern void darkcyan();
extern void purple();
extern void black();
extern void white();
extern void grey();
extern void setRGBFg(unsigned int r, unsigned int g, unsigned int b);
extern void setHexFg(char * hexColor);
extern void reset();


extern void fgreen(FILE *file);
extern void fred(FILE *file);
extern void fblue(FILE *file);
extern void fyellow(FILE *file);
extern void fpink(FILE *file);
extern void fgray(FILE *file);
extern void fdarkgray(FILE *file);
extern void forange(FILE *file);
extern void fdarkblue(FILE *file);
extern void fcyan(FILE *file);
extern void flightcyan(FILE *file);
extern void fdarkcyan(FILE *file);
extern void fpurple(FILE *file);
extern void fblack(FILE *file);
extern void fwhite(FILE *file);
extern void fgrey(FILE *file);
extern void fsetRGBFg(FILE *file, unsigned int r, unsigned int g, unsigned int b);
extern void fsetHexFg(FILE *file,char * hexColor);
extern void freset(FILE *file);
