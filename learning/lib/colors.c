#include <custom/colors.h>
#include <stdio.h>

void green() { printf("\033[38;2;0;255;0m"); }

void red() { printf("\033[38;2;255;0;0m"); }

void blue() { printf("\033[38;2;0;0;255m"); }

void yellow() { printf("\033[38;2;255;255;0m"); }

void pink() { printf("\033[38;2;255;192;203m"); }

void gray() { printf("\033[38;2;128;128;128m"); }

void darkgray() { printf("\033[38;2;169;169;169m"); }

void orange() { printf("\033[38;2;255;165;0m"); }

void darkblue() { printf("\033[38;2;0;0;139m"); }

void cyan() { printf("\033[38;2;0;255;255m"); }

void lightcyan() { printf("\033[38;2;224;255;255m"); }

void darkcyan() { printf("\033[38;2;0;139;139m"); }

void purple() { printf("\033[38;2;128;0;128m"); }

void black() { printf("\033[38;2;0;0;0m"); }

void white() { printf("\033[38;2;255;255;255m"); }

void grey() { printf("\033[38;2;128;128;128m"); }

void reset() { printf("\033[0m"); }

void setRGBFg(unsigned int r, unsigned int g, unsigned int b) {
  printf("\033[38;2;%u;%u;%um", r, g, b);
}

void setHexFg(char *hexColor) {
  unsigned int r, g, b;
  sscanf(hexColor, "%02x%02x%02x", &r, &g, &b);
  setRGBFg(r, g, b);
}


void fgreen(FILE *file) { fprintf(file,"\033[38;2;0;255;0m"); }

void fred(FILE *file) { fprintf(file,"\033[38;2;255;0;0m"); }

void fblue(FILE *file) { fprintf(file,"\033[38;2;0;0;255m"); }

void fyellow(FILE *file) { fprintf(file,"\033[38;2;255;255;0m"); }

void fpink(FILE *file) { fprintf(file,"\033[38;2;255;192;203m"); }

void fgray(FILE *file) { fprintf(file,"\033[38;2;128;128;128m"); }

void fdarkgray(FILE *file) { fprintf(file,"\033[38;2;169;169;169m"); }

void forange(FILE *file) { fprintf(file,"\033[38;2;255;165;0m"); }

void fdarkblue(FILE *file) { fprintf(file,"\033[38;2;0;0;139m"); }

void fcyan(FILE *file) { fprintf(file,"\033[38;2;0;255;255m"); }

void flightcyan(FILE *file) { fprintf(file,"\033[38;2;224;255;255m"); }

void fdarkcyan(FILE *file) { fprintf(file,"\033[38;2;0;139;139m"); }

void fpurple(FILE *file) { fprintf(file,"\033[38;2;128;0;128m"); }

void fblack(FILE *file) { fprintf(file,"\033[38;2;0;0;0m"); }

void fwhite(FILE *file) { fprintf(file,"\033[38;2;255;255;255m"); }

void fgrey(FILE *file) { fprintf(file,"\033[38;2;128;128;128m"); }

void freset(FILE *file) { fprintf(file,"\033[0m"); }

void fsetRGBFg(FILE *file, unsigned int r, unsigned int g, unsigned int b) {
  fprintf(file,"\033[38;2;%u;%u;%um", r, g, b);
}

void fsetHexFg(FILE *file,char *hexColor) {
  unsigned int r, g, b;
  scanf(hexColor, "%02x%02x%02x", &r, &g, &b);
  fsetRGBFg(file, r, g, b);
}
