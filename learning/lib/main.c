#include "colors.h"

int main(void) {
  red();
  printf("This a red text\n");
  blue();
  printf("This is a blue text\n");
  green();
  printf("This is a green text\n");
  yellow();
  printf("This is a yellow text\n");
  pink();
  printf("This is a pink text\n");
  darkcyan();
  printf("This is a darkcyan text\n");
  reset();

  return 0;
}

