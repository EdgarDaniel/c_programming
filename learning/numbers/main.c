#include <limits.h>
#include <locale.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

struct winsize w;

void repeatchar(short count, char character) {
  puts("");
  for (short i = 1; i <= count; i++) {
    putchar(character);
  }
  puts("\n");
}

void minmax() {
  repeatchar(w.ws_col, '=');
  char *current = "INT";
  printf("For %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %d\n", current, INT_MAX);
  printf("\t‣%s (minium of signed integer): %d\n", current, INT_MIN);

  current = "INT8";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %d\n", current, INT8_MAX);
  printf("\t‣%s (minium of signed integer): %d\n", current, INT8_MIN);

  current = "INT16";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %d\n", current, INT16_MAX);
  printf("\t‣%s (minium of signed integer): %d\n", current, INT16_MIN);

  current = "INT32";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %d\n", current, INT32_MAX);
  printf("\t‣%s (minium of signed integer): %d\n", current, INT32_MIN);

  current = "INT64";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %ld\n", current, INT64_MAX);
  printf("\t‣%s (minium of signed integer): %ld\n", current, INT64_MIN);

  current = "UINT";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %u\n", current, UINT_MAX);
  printf("\t‣%s (minium of signed integer): %u\n", current, 0);

  current = "UINT8";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %u\n", current, UINT8_MAX);
  printf("\t‣%s (minium of signed integer): %u\n", current, 0);

  current = "UINT16";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %u\n", current, UINT16_MAX);
  printf("\t‣%s (minium of signed integer): %u\n", current, 0);

  current = "UINT32";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %u\n", current, UINT32_MAX);
  printf("\t‣%s (minium of signed integer): %u\n", current, 0);

  current = "UINT64";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %lu\n", current, UINT64_MAX);
  printf("\t‣%s (minium of signed integer): %u\n", current, 0);

  current = "LONG";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %ld\n", current, LONG_MAX);
  printf("\t‣%s (minium of signed integer): %ld\n", current, LONG_MIN);

  current = "ULONG";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %zu\n", current, ULONG_MAX);
  printf("\t‣%s (minium of signed integer): %d\n", current, 0);

  current = "SHORT";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %d\n", current, SHRT_MAX);
  printf("\t‣%s (minium of signed integer): %d\n", current, SHRT_MIN);

  current = "USHORT";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %u\n", current, USHRT_MAX);
  printf("\t‣%s (minium of signed integer): %u\n", current, 0);

  current = "CHAR";
  printf("\nFor %s:\n", current);
  printf("\t‣%s (maxium of signed integer): %d\n", current, CHAR_MAX);
  printf("\t‣%s (minium of signed integer): %d\n", current, CHAR_MIN);

  repeatchar(w.ws_col, '=');
}

void countbytes(void) {

  // Some examples like: int16_t are for signed numbers
  // whereas uint16_t are for unsigned numbers
  // so if in these examples the used type is 'int8_t' the max value showed
  // should be substracted 1 and divided by two if result shows:   65535 then it
  // should be the result: (65535-1)/2
  //  which gives: 32,767

  char *current = "int";
  repeatchar(w.ws_col, '=');

  printf("sizeof(%s) = %zu\n", current, sizeof(int));
  size_t maxOnes = sizeof(int) * 8;
  size_t max_val = (1UL << maxOnes) - 1;
  printf("max bits (unsigned): %zu = %zu\n", maxOnes, max_val);

  // current = "unsigned int";
  // printf("\nsizeof(%s) = %zu\n", current, sizeof(unsigned int));
  // maxOnes = sizeof(unsigned int) * 8;
  // if (maxOnes == 64) {
  //   max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  // } else {
  //   max_val = (1UL << maxOnes) - 1;
  // }
  // printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "short";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(short));
  maxOnes = sizeof(short) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "long";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(long));
  maxOnes = sizeof(long) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  // max_val = ( UINT64_C(1) << 64) - 1;
  // max_val = (( 1ULL << 63 ))*2-1;
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "long long";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(long long));
  maxOnes = sizeof(long long) * 8;
  // max_val = (1UL << maxOnes) - 1;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "long long int";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(long long int));
  maxOnes = sizeof(long long int) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "long int";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(long int));
  maxOnes = sizeof(long int) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "uint8_t";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(uint8_t));
  maxOnes = sizeof(uint8_t) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "uint16_t";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(uint16_t));
  maxOnes = sizeof(uint16_t) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "uint32_t";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(uint32_t));
  maxOnes = sizeof(uint32_t) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "uint64_t";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(uint64_t));
  maxOnes = sizeof(uint64_t) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "size_t (unsigned)";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(size_t));
  maxOnes = sizeof(size_t) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "ssize_t";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(ssize_t));
  puts("ssize_t is signed so the result should be multplied by 2");
  maxOnes = sizeof(ssize_t) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "char";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(char));
  maxOnes = sizeof(char) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "char *";
  printf("\n(not relevant) sizeof(%s) = %zu\n", current, sizeof(char *));
  maxOnes = sizeof(char *) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "char **";
  printf("\n(not relevant) sizeof(%s) = %zu\n", current, sizeof(char **));
  maxOnes = sizeof(char **) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  current = "char[100]";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(char[100]));
  maxOnes = sizeof(char[100]) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits: (unsigned) %zu = %zu\n", maxOnes, max_val);

  // current = "charvar";
  // puts("charvar is a variable = char * charvar = \"Hello everybody, using
  // Linux :D\""); char * charvar = "Hello everybody, using Linux :D";
  // printf("\nstrlen(%s) = %zu\n", current, strlen(charvar));
  // maxOnes = strlen(charvar) * 8;
  // if (maxOnes == 64) {
  //   max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  // } else {
  //   max_val = (1UL << maxOnes) - 1;
  // }

  current = "float";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(float));
  maxOnes = sizeof(float) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits:  %zu = (%zu-1)/2 = %zu\n", maxOnes, max_val,
         (max_val - 1) / 2);

  current = "double";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(double));
  maxOnes = sizeof(double) * 8;
  if (maxOnes == 64) {
    max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  } else {
    max_val = (1UL << maxOnes) - 1;
  }
  printf("max bits:  %zu = (%zu-1)/2 = %zu\n", maxOnes, max_val,
         (max_val - 1) / 2);

  current = "long double";
  printf("\nsizeof(%s) = %zu\n", current, sizeof(long double));
  maxOnes = sizeof(long double) * 8;
  // if (maxOnes == 64) {
  //   max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  // } else if (maxOnes == 128) {
  //   max_val = (1UL << (maxOnes - 1)) * 2 - 1;
  // } else {
  //   max_val = (1UL << maxOnes) - 1;
  // }
  printf("max bits:  %zu\n", maxOnes);
  // double dPI = 3.1415926535897932384633545045495940594012121212121904390495903940349;
  // long double ldPI = 3.1415926535897932384633545045495940594012121212121904390495903940349;

  // printf("double PI = %.30f and long double PI = %.30Lf\n", dPI, ldPI);
  printf("The Value of Pi here is %.50Lf\n", M_PI);

  repeatchar(w.ws_col, '=');
}

int main(void) {

  ioctl(STDIN_FILENO, TIOCGWINSZ, &w);
  int opc, nitems, ch;

  // size_t max_valst = 18446744073709551615UL;
  // unsigned __int128 a = 18446744073709551615UL+10000UL;
  // printf("%x\n",a);
  // printf("%lu\n",max_valst);
  // setlocale(LC_ALL, "");
  printf("\u2500\u2501\n");
  // unsigned char sr = 'Ç';
  unsigned char welcome_char = 128;
  printf("128 ascii is %c\n", 128);
  puts("Running\vprogram");

  while (1) {

    puts("1) MinMax values");
    puts("2) Count bytes size");
    puts("?) Exit");
    printf("Choose and option> ");
    nitems = scanf(" %d", &opc);

    if (nitems == EOF) {
      puts("Failed to read stdin");
      break;
    } else if (nitems == 0) {
      getchar();
      puts("Not items matched");
      while ((ch = getchar()) != '\n' && ch != EOF)
        ;
    } else {
      switch (opc) {
      case 1:
        minmax();
        break;
      case 2:
        countbytes();
        break;
      default:
        return 0;
      }
    }
  }
}
