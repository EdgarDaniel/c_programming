#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STR_SIZE 10

char *printfFlagSize() {
  char *data = malloc(sizeof(*data) * STR_SIZE);
  if (data != NULL) {
    sprintf(data, "%%%ds", STR_SIZE - 1);
  } else {
    strerror(errno);
    exit(1);
  }
  return data;
}

void readSafeString() {
  char name[STR_SIZE];
  printf("Type a %d-length or minus string (without spaces): ", STR_SIZE - 1);
  char *flag = printfFlagSize();
  scanf(flag, name);

  printf("[len: %zu,sizeof: %zu] Your 9-length name is: %s\n", strlen(name),
         sizeof(name), name);
  free(flag);
  flag = NULL;
}

void readSafeStringLine() {
  char name[40];
  printf("What's your name? ");
  if (fgets(name, 40, stdin)) {
    name[strcspn(name, "\n")] = 0;
    printf("Hello %s!\n", name);
  }
}

void readSafeNumber() {
  long a;
  char buf[1024]; // use 1KiB just to be sure
  int success;    // flag for successful conversion

  do {
    printf("enter a number: ");
    if (!fgets(buf, 1024, stdin)) {
      // reading input failed:
      fprintf(stderr, "Failed to read.");
      return;
    }

    // have some input, convert it to integer:
    char *endptr;

    errno = 0; // reset error number
    a = strtol(buf, &endptr, 10);
    if (errno == ERANGE) {
      printf("Sorry, this number is too small or too large.\n");
      success = 0;
    } else if (endptr == buf) {
      // no character was read
      success = 0;
    } else if (*endptr && *endptr != '\n') {
      // *endptr is neither end of string nor newline,
      // so we didn't convert the *whole* input
      success = 0;
    } else {
      success = 1;
    }
  } while (!success); // repeat until we got a valid number

  printf("You entered %ld.\n", a);
}

int main(void) {
  // readSafeString();
  // readSafeStringLine();
  // char chars[50] = "Something ";
  // printf("Concat: %s\n", strcat(strcat(chars, "well")," or bad?") );
  readSafeNumber();
  return 0;
}
