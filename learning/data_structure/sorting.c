#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int cmpfunc(const void *a, const void *b) { return (*(int *)a - *(int *)b); }
int cmp_str_func(const void *a, const void *b) {
  // char *const *pp1 = a;
  // char *const *pp2 = b;
  // return strcmp(*pp1, *pp2);
  return strcmp( *((char **)a), *((char **)b));
}

int main(void) {
  int values[] = {88, 56, 100, 2, 25};
  int n;

  printf("Before sorting the list is: \n");
  for (n = 0; n < 5; n++) {
    printf("%d ", values[n]);
  }

  qsort(values, 5, sizeof(values[0]), cmpfunc);

  printf("\nAfter sorting the list is: \n");
  for (n = 0; n < 5; n++) {
    printf("%d ", values[n]);
  }
  puts("");

  char *strings[] = {"Adrian",    "Sofia", "Bere",
                     "Alejandra", "Zara",  "Cristina"};

  printf("\nBefore sorting the list is: \n");
  for (n = 0; n < 6; n++) {
    printf("%s ", strings[n]);
  }

  qsort(strings, 6, sizeof(char *), cmp_str_func);

  printf("\nAfter sorting the list is: \n");
  for (n = 0; n < 6; n++) {
    printf("%s ", strings[n]);
  }
  puts("");
  return (0);
}
