#include <custom/colors.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
  struct Node *left, *right;
  int data;
} TreeNode;

TreeNode *root;

void addNode(int data){
  if(root==NULL){
    root = malloc(sizeof(*root));
    root->data = data;
    root->left = NULL;
    root->right = NULL;
  }else{
    TreeNode *current = root, *tmp;

    while (1) {
      if(current->data>data){
        if(current->left == NULL){
            tmp = malloc(sizeof(*tmp));
            tmp->data = data;
            current->left = tmp;
            break;

        }
        else current = current->left;
      }else{
        if(current->right == NULL){
            tmp = malloc(sizeof(*tmp));
            tmp->data = data;
            current->right = tmp;
            break;
        }
        else tmp = current->right;
      }
    }
  }
}

void inOrder(TreeNode *node){
  if(node == NULL)
    return;

  inOrder(node->left);
  printf("%d, ",node->data);
  inOrder(node->right);
}

void preOrder(TreeNode *node){
  if(node == NULL)
    return;

  printf("%d, ",node->data);
  preOrder(node->left);
  preOrder(node->right);
}

void postOrder(TreeNode *node){
  if(node == NULL)
    return;

  postOrder(node->left);
  postOrder(node->right);
  printf("%d, ",node->data);
}

void printInOrder(){
  lightcyan();
  puts("\nInOrder:");
  inOrder(root);
  puts("\n");
  reset();
}

void printPreOrder(){
  cyan();
  puts("\nPreOrder:");
  preOrder(root);
  puts("\n");
  reset();
}

void printPostOrder(){
  orange();
  puts("\nPostOrder:");
  postOrder(root);
  puts("\n");
  reset();
}

void freeAlloc(TreeNode *node){
  if(node == NULL)
    return;

  freeAlloc(node->left);
  freeAlloc(node->right);
  free(node);
}

int main(void)
{
  root = NULL;
  addNode(70);
  printf("Root: %d\n",root->data);
  addNode(50);
  printf("Root.left: %d\n",root->left->data);
  addNode(40);
  printf("Root.left.left: %d\n",root->left->left->data);
  addNode(80);
  printf("Root.right: %d\n",root->right->data);
  addNode(60);
  printf("Root.left.right: %d\n",root->left->right->data);
  printInOrder();
  printPreOrder();
  printPostOrder();
  freeAlloc(root);
  return EXIT_SUCCESS;
}
