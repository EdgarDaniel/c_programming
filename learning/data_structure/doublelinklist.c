#include <custom/colors.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
  struct Node *next, *prev;
  int data;
} Node;

Node *head, *tail;

void showReverse() {
  struct Node *list = tail;
  if (head == NULL) {
    fred(stderr);
    fprintf(stderr, "List is empty\n");
    reset();
    return;
  }
  yellow();
  puts("\n================================Reverse "
       "List================================="
       "=======\n");
  cyan();
  while (list != NULL) {
    printf("%d, ", list->data);
    list = list->prev;
  }
  yellow();
  puts("\n====================================================================="
       "=======\n\n");
  reset();
}

void showList() {
  struct Node *list = head;
  if (head == NULL) {
    fred(stderr);
    fprintf(stderr, "List is empty\n");
    reset();
    return;
  }
  orange();
  puts("\n================================List================================="
       "=======\n");
  cyan();
  while (list != NULL) {
    printf("%d, ", list->data);
    list = list->next;
  }
  orange();
  puts("\n====================================================================="
       "=======\n\n");
  reset();
}

void addLast(int data) {
  if (head == NULL) {
    head = malloc(sizeof(*head));
    head->next = NULL;
    head->prev = NULL;
    head->data = data;
    tail = head;
  } else {
    Node *tmp = malloc(sizeof *tmp);
    tmp->data = data;
    tmp->next = NULL;
    tail->next = tmp;
    tmp->prev = tail;
    tail = tmp;
  }
}

void addFirst(int data) {

  if (head == NULL) {
    head = malloc(sizeof(*head));
    head->next = NULL;
    head->prev = NULL;
    head->data = data;
    tail = head;
  } else {
    Node *tmp = malloc(sizeof *tmp);
    tmp->data = data;
    tmp->next = head;
    tmp->prev = NULL;
    head->prev = tmp;
    head = tmp;
  }
}

void deleteLast() {
  if (head == NULL) {
    fred(stderr);
    fprintf(stderr, "List is empty\n");
    reset();
    return;
  }

  printf("Removed: %d\n", tail->data);

  struct Node *temp = tail;
  if (head->next == NULL) {
    head = tail = NULL;
  }else{
  tail = tail->prev;
  tail->next = NULL;
  }
  free(temp);
}

void deleteFirst() {

  if (head == NULL) {
    fred(stderr);
    fprintf(stderr, "List is empty\n");
    reset();
    return;
  }

  printf("Removed: %d\n", head->data);

  struct Node *temp = head;
  if (head->next == NULL) {
    head = tail = NULL;
  } else {
    head = head->next;
    head->prev = NULL;
  }
  free(temp);
}

void freealloc() {
  if (head == tail) {
    free(head);
  } else {
    Node *tmp = head;
    while (tmp->next != NULL) {
      tmp = tmp->next;
      free(tmp->prev);
    }
    free(tmp);
    tmp = NULL;
  }
}

int main(void) {
  printf("Sizeof Node: %zu\n", sizeof(Node));
  head = tail = NULL;
  addLast(89);
  addLast(97);
  addFirst(100);
  addLast(-7);
  addLast(-70);
  addFirst(45);
  addLast(8);
  addLast(-98);
  addFirst(255);
  showList();
  showReverse();

  deleteLast();
  deleteLast();
  deleteLast();
  showList();
  deleteFirst();
  deleteFirst();
  deleteFirst();
  showList();
  deleteFirst();
  deleteFirst();
  deleteFirst();
  showList();

  freealloc();
  return EXIT_SUCCESS;
}
