#include <custom/colors.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
  struct Node *next;
  int data;
} Node;

Node *head, *tail;

void __reverse(Node * node){
  if(node==NULL)
    return;

  __reverse(node->next);
  printf("%d, ",node->data);
}

void showReverse(){
  green();
  puts("\nList in reverse order:");
  __reverse(head);
  reset();
  puts("\n");
}


void showList() {
  struct Node *list = head;
  if (head == NULL) {
    fred(stderr);
    fprintf(stderr, "List is empty\n");
    reset();
    return;
  }
  orange();
  puts("\n================================List================================="
       "=======\n");
  cyan();
  while (list != NULL) {
    printf("[%p] %d, ",list, list->data);
    list = list->next;
  }
  orange();
  puts("\n====================================================================="
       "=======\n\n");
  reset();
}

void showFirst() { printf("First element: %d\n", head->data); }

void showLast() { printf("Last element: %d\n", tail->data); }

// this adds at the end
void add(int data) {
  if (head == NULL) {
    head = malloc(sizeof(Node));
    head->data = data;
    head->next = NULL;
    tail = head;
  } else {
    struct Node *tmp = malloc(sizeof(Node));
    tmp->data = data;
    tmp->next = NULL;
    tail->next = tmp;
    tail = tmp;
  }
}

// (head  is local to the function and  is passed by value
// and not by reference, its not passing the address) not sure about this
// However, since head is allocating and creatinng a new Node pointer, if we did not return
// the pointer then the assigment would have no effect on the head.
// 
// In a function, changes to pointer variables or pointer parameters have no effect outside the function. 
// However, if the pointer is pointing to an object outside the function, that object can be modified by dereferencing the pointer.
// https://stackoverflow.com/questions/61481180/change-passed-pointer-in-function
Node *addLast(Node *head, int data) {
  if (head == NULL) {
    head = malloc(sizeof(Node));
    head->data = data;
    head->next = NULL;
    tail = head;
  } else {
    struct Node *tmp = malloc(sizeof(Node));
    tmp->data = data;
    tmp->next = head;
    head = tmp;
  }
  return head;
}

void addFirst_v2(int data) {

  if (head == NULL) {
    head = malloc(sizeof(Node));
    head->data = data;
    head->next = NULL;
    tail = head;
  } else {
    struct Node *tmp = malloc(sizeof(Node));
    tmp->data = data;
    tmp->next = head;
    head = tmp;
  }
}

// this is passed by reference
void addFirst_v3(Node **head, int data) {
  if (head == NULL) {
    head = malloc(sizeof(Node));
    *&(*head)->data = data;
    (*head)->next = NULL;
    // *&(*head)->next = NULL;
    tail = *head;
  } else {
    struct Node *tmp = malloc(sizeof(Node));
    tmp->data = data;
    tmp->next = *head;
    *head = tmp;
  }
}

void deleteFirst() {
  if (head == NULL) {
    fred(stderr);
    fprintf(stderr, "List is empty\n");
    reset();
    return;
  }

  printf("Removed first: %d\n", head->data);
  // this is not freeing memory:
  // if(head->next!=NULL)
  //   head = head->next;
  // else{
  //   head = tail = NULL;
  // }

  struct Node *temp = head;
  if (head->next == NULL) {
    head = tail = NULL;
  } else {
    head = head->next;
  }
  free(temp);
  temp = NULL;
}

void deleteLast() {
  if (head == NULL) {
    fred(stderr);
    fprintf(stderr, "List is empty\n");
    reset();
    return;
  }

  struct Node *list = head, *pre = head;
  // int count=0;

  while (list->next != NULL) {
    pre = list;
    list = list->next;
    // count++;
  }

  // printf("Count: %d with pre: %d\n",count,pre->data);
  printf("Removed last: %d\n", list->data);
  if (list == head) {
    head = tail = NULL;
  } else {
    tail = pre;
    tail->next = NULL;
  }
  free(list);
  list = NULL;
}

int main(void) {
  head = tail = NULL;

  printf("Sizeof Node: %zu\n",sizeof(Node));

  add(90);
  showList();
  // add(95);
  // showList();
  deleteLast();
  showList();
  add(89);
  showList();
  add(10);
  add(25);
  showList();
  // showList();
  head = addLast(head, 250);
  // showList();
  addFirst_v2(-1);
  // showList();
  addFirst_v3(&head, 909);
  addFirst_v3(&head, 057);
  addFirst_v3(&head, 0xFF);
  showList();
  showReverse();
  deleteLast();
  deleteLast();
  deleteLast();
  deleteLast();
  showList();
  deleteFirst();
  showList();
  deleteFirst();
  deleteFirst();
  showList();
  deleteFirst();
  deleteFirst();
  deleteLast();
  addFirst_v3(&head, 067);
  addFirst_v3(&head, 012);
  addFirst_v3(&head, 0122);
  showList();
  deleteLast();
  // deleteLast();
  // deleteLast();
  showList();

  if(head == tail){
    free(head);
  }else{
    free(head);
    free(tail);
  }
  return 0;
}
