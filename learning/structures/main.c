#include <custom/colors.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Car is a tag
struct Car {
  int ID;
  char *model;
} car1, car2, carDefault = {0, "None"};

struct {
  char *name;
} withoutTag;

typedef struct Person {
  char *job;
} PersonObj, AliasPersonObj;

// Using typedef
typedef struct Car CarObject;

void printStrcWoTag() {
  puts("\n=================No tag=========================\n");
  // struct withoutTag;
  withoutTag.name = "I'm a structure without tags";
  printf("withoutTag.name = %s\n", withoutTag.name);
  puts("\n==========================================\n");
}

void printCarStrc(struct Car car) {
  puts("\n================Struct Car==========================\n");
  printf("CarID[%d] and is %s\n", car.ID, car.model);
  puts("\n==========================================\n");
}

void printCarType(CarObject car) {
  puts("\n================Type Car aka CarObject==========================\n");
  printf("CarID[%d] and is %s\n", car.ID, car.model);
  puts("\n==========================================\n");
}

// here: AliasPersonObj  is equivalent to PersonObj:
// void printPerson(PersonObj p) {
void printPerson(AliasPersonObj p) {
  puts("\n================Type Person aka "
       "PersonObj==========================\n");
  printf("Person is %s\n", p.job);
  puts("\n==========================================\n");
}

void normalStructs() {

  car1.ID = 1;
  car1.model = "Audi";
  car2.ID = 2;
  car2.model = "Civic";

  struct Car car3 = {3, "Lamborguini"};

  struct Car car4;
  car4.ID = 4;
  car4.model = "Ferrari";

  typeof(car4.ID) var = 9;
  printf("typeof(car4.ID) var has as value: %d\n", var);

  printCarStrc(car1);
  printCarStrc(car2);
  printCarType(car3);
  printCarType(car4);
  printCarStrc(carDefault);
  printStrcWoTag();

  struct Person employer;
  PersonObj manager = {.job = "Boss"};
  printPerson(manager);
  employer.job = "Backend Developer";
  manager.job = "Manager";
  printPerson(employer);
  printPerson(manager);
}

typedef struct TableUser {
  char *name;
  char *lastname;
  unsigned int age;
} User;

void showTableUsers(User *user) {
  puts("\nGetting User:");
  yellow();
  printf("\tName: %s", user->name);
  printf("\tLastname: %s\n", user->lastname);
  printf("\tAge: %u\n", user->age);
  reset();
  puts("\n");
}

struct Categories {
  char name[20];
  char descripcion[100];
} *pointerCat;

void editCategorie(struct Categories *cat) {
  printf("Type the Categorie name and its Description: ");
  scanf(" %s %s", cat->name, cat->descripcion);
}

void printCategorie(struct Categories cat, char *title) {
  printf("\nCategorie %s:\n", title);
  setRGBFg(152, 190, 101);
  printf("\tName: %s\n", cat.name);
  printf("\tDescription: %s\n", cat.descripcion);
  reset();
  puts("\n");
}

void pointerStructsProps() {

  struct Categorie2 {
    char name[50];
    char descripcion[100];
    unsigned int genero : 1;
  };

  struct Categorie2 cat = {"Accesorios", "Relojs, pulseras y collares"};
  cat.genero = 0;

  struct Categorie2 *pointer = &cat;

  gray();
  puts("\n\nAccessing props:");
  darkcyan();
  printf("\t[With pointer>prop] Name: %s\n", pointer->name);
  printf("\t[With (*pointer).prop] Description: %s\n", (*pointer).descripcion);
  printf("\t[With (&cat).prop] Genero: %u\n", (&cat)->genero);
}

void pointers2Structs() {

  struct Categories cat1 = {"Eletronicos", "TVs, celulares y laptops"};

  struct Categories cat2 = {"Default", "Default"};

  struct Categories cat3;

  pointerCat = malloc(sizeof(*pointerCat));
  strcpy(pointerCat->descripcion, "Lavadoras,refrigeradores");
  strcpy(pointerCat->name, "Domesticos");

  printCategorie(cat1, "#1");
  printCategorie(cat2, "#2");
  printCategorie(*pointerCat, "pointer");
  editCategorie(&cat2);
  editCategorie(pointerCat);
  printCategorie(*pointerCat, "pointer");

  // printf("\nGive a categorie name: ");
  // this is ilegal, core dumped:
  // char * catName;
  // scanf(" %s",catName);
  // printCategorie(cat2, catName);

  printCategorie(cat2, "#2");

  editCategorie(&cat3);
  printCategorie(cat3, "#3");
}

#define MAX_USERS 0

void arrayStructs() {
  // printf with colors
  setRGBFg(71, 165, 229);
  puts("\n\nArray of structs");

  reset();
  User *users[MAX_USERS];
  // memset(users, 0, sizeof(users) * MAX_USERS);
  // users = malloc(sizeof(User)*MAX_USERS);

  for (size_t i = 0; i < MAX_USERS; i++) {
    users[i] = malloc(sizeof(User));
    users[i]->name = malloc(sizeof(char *));
    // Dont use this:
    // users[i]->name = "Pedro";
    // Use this:
    // strcpy(users[i]->name, "Pedro");
    users[i]->lastname = malloc(sizeof(char *));
    printf("[%zu] Type user name, lastname, and age: ", i);
    scanf(" %s %s %u", users[i]->name, users[i]->lastname, &users[i]->age);
  }

  for (size_t i = 0; i < MAX_USERS; i++) {
    showTableUsers(users[i]);
  }
}

// usin this the wasted memory will be lower (efficient)
// but the cpy cycles will be wasted
// #pragma pack(1)

void padding() {
  struct abc {
    char a; // 1 byte
    char b; // 1 byte
    int c;  // 4 bytes
  };

  cyan();
  printf("\nPadding:\n");
  orange();
  puts("Struct:");
  puts("struct abc {\n"
       "\tchar a; // 1 byte\n"
       "\tchar b; // 1 byte\n"
       "\tint c; // 4 bytes\n"
       "}");
  setRGBFg(239, 107, 90);
  printf("\tsizeof(abc): %zu\n", sizeof(struct abc));
  // The struct above does not have a size of 6 bytes
  // See:
  // /home/edgar/Development/Gitlab/c_programming/learning/structures/padding.png
  //  and
  //  https://www.youtube.com/watch?v=aROgtACPjjg&list=PLBlnK6fEyqRhX6r2uhhlubuF5QextdCSM&index=158

  orange();
  struct def {
    char a; // 1 byte
    int b;  // 4 bytes
    char c; // 1 byte
  };
  puts("\nStruct:");
  puts("struct def {\n"
       "\tchar a; // 1 byte\n"
       "\tint b; // 4 byte\n"
       "\tchar c; // 1 bytes\n"
       "}");
  setRGBFg(239, 107, 90);
  printf("\tsizeof(def): %zu\n", sizeof(struct def));

  orange();
  struct __attribute__((__packed__)) ghi {
    char a; // 1 byte
    int b;  // 4 bytes
    char c; // 1 byte
  };
  puts("\nStruct:");
  puts("struct  __attribute__((__packed__)) ghi {\n"
       "\tchar a; // 1 byte\n"
       "\tint b; // 4 byte\n"
       "\tchar c; // 1 bytes\n"
       "}");
  setRGBFg(239, 107, 90);
  printf("\tsizeof(def): %zu\n", sizeof(struct ghi));
}

void accessbyadress(){
  struct OurNode {
    int a;
    char x,y,z;
  };

  struct OurNode p = {24,'J','U','a'+2};

  struct OurNode *q=&p; // q (or &p) points to first variable value, which is 'x' and has J
 
  yellow();
  puts("\n\nAccess before: *(int *)q = 1009;");
  printf("\t[using *q ] a: %d\n",*q);
  printf("\t[using 0[q] ] a: %d\n",0[q]);
  printf("\t[using 'int *' as cast to *q: *((int *)q) ] a: %d\n",*((int *)q));
  
  *(int *)q = 1009;
  puts("\n\nAccess after: *(int *)q = 1009;");
  printf("\t[using 'int *' as cast to *q: *((int *)q) ] a: %d\n",*((int *)q));

  printf("\t[using ((char *)q+sizeof(int)) ] x: %c\n",*( (char *) q+sizeof(int) ) );
  printf("\t[using ((char *)q+sizeof(int)+1) ] x: %c\n",*( (char *) q+sizeof(int)+1 ) );
  printf("\t[using ((char *)q+sizeof(int)+1+1) ] x: %c\n",*( (char *) q+sizeof(int)+1+1 ) );
}

int main(void) {
  normalStructs();
  arrayStructs();
  pointers2Structs();
  pointerStructsProps();
  padding();
  accessbyadress();
}
