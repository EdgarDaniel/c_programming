#include <stdio.h>

// TODO Compile the file and run the command 'size' in the terminal passing the exectuable file as argument:
//  size ./alone.run

// is stored in data segement
static int x=10;
static int y;
static int y=23;
static int y;
// with static int x=0 the variable is stored in bss (check output with size command), this happens
// since non-intialized global/static variables are assigened to 0 by default
// static int x=0;
int main(void)
{
  // static int x;
  return 0;
} 
