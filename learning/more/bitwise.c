#include <stdio.h>

void  swapWxor(){
  int x=10,y=12;
  printf("x and y before xor swap: %d,%d\n",x,y);
  x = x ^ y;
  y = x ^ y;
  x = x ^ y;
  printf("x and y now: %d,%d\n",x,y);
}

int main(void)
{
  int var = 3,var2=4;
  printf("\n3 in binary: %08b\n",3);
  printf("Left shift %1$08b << 1 = %2$08b => Decimal: %2$d\n",var, var<<1);

  puts("\nLeft shifting is equivalent to multiplication by 2^(righOperand):");
  puts("\tSo, 3 << 1 = 3x2^1 = 6");

  puts("\nRight shift operator:");
  printf("\t %1$08b >> 1 = %2$08b => Decimal: %2$d\n",var, var>>1);
  printf("\t %1$08b >> 4 = %2$08b => Decimal: %2$d\n",32, 32>>4);

  puts("\tRigth shifting is equivalent to division by 2^(righOperand):");
  puts("\tSo, 3 >> 1 = 3/2^1 = 1 (1.5 but the decimal point is removed)");

  puts("\nXOR operator:");
  printf("\t%1$08b ^ %2$08b = %3$08b => Decimal: %3$d\n",var,var2,var^var2);

  swapWxor();
  return 0;
}
