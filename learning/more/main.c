#include "vars.h"
#include <stdio.h>

int globalunitialized;
char *OS_VERSION;
extern int bmain;
// it's allowed to declare multiple variable (with same name) since it's only
// declarating and not defining it
extern int bmain;
 int cnormal = 900;
// Marks warning:
// extern int dmain = -88;

#define add(x,y,z) x+y+z 
#define parenthadd(x,y,z) (x+y+z)
#define max(x,y) if (x>y) \
          printf("%d is greater than %d\n",x,y);\
          else \
          printf("%d is lesser than %d\n",x,y);

int fn3(int,char);
int fn3(int x,char y){
  return (x+y)*30/8;
}

int fn1(){
  puts("Linux");
  return 2;
}

int fn2(){
  puts("Unix");
  return 3;
}


void precedence(){
  puts("\n\nPrecedence of operators");
  // here first prints Linux (call fn1 first)
  int v = fn1() + (fn2()*1);
  printf("Sum: %d\n",v);
  // here it calls fn2 first
  v = fn1() + (fn2()*0);
  printf("Sum: %d\n",v);

  puts("\n[C99 standard] Inside of sizeof(var/expr) if an expr is given (and is not a variable length array type) then expr is not evaluated");
  puts("\nFor example, having int x = 10 ; sizeof(x++): the 'x++' will not be evaluated and only sizeof will return the size of x variable type (int)");
  int x = 10;
  int var = sizeof(x++);
  printf("x and var: %d,%d\n",x,var);
}

void commaoperator(){

  int x = (
      printf("%s\n","I'm inside comma operator assigment"),
      10+5
  );
  printf("x from comma operator: %d\n",x);
 
  int a = (3,5,10);
  printf("a from comma operator '(3,5,10)' = %d\n",a);

  a = 13,51,100;
  printf("a from comma operator '13,51,100': %d\n",a);

  // This throws an error:
    // int b = 10,24,102;
  // since acts as a separator and is equivalent to int b = 10; int 24; int 102;
}

void macros(){
  puts("\n\nMacros");
  printf("__DATE__ and __TIME__ = (%s,%s)\n",__DATE__,__TIME__);
  puts("\tUsing macro: add(x,y,z) => x+y+z");
  printf("\t\tResult of add(10.23,34.32,1.0) = %f\n",add(10.23,34.32,1.0));
  printf("\t\tResult of add(1,-10,3)) = %d\n",add(1,-10,3));
  printf("\t\tResult of 2 * add(3,5,2) = %d\n",2 * add(3,5,2));
  printf("\t\tResult of 2 * parenthadd(3,5,2) = %d\n",2 * parenthadd(3,5,2));
  puts("\tUsing macro max(x,y):");
  // printf("\t\tmax(10,234) = %d\n",
  max(10,234);
  max(10,-234);
}

void registers(){
  puts("\nUsing registers:");
  puts("\tThe syntax is: register datatype variable_name");
  puts("\t  register int reg1 = 100;");
  puts("\tWith 'register' hits the compiler to store a variable in register memory");
  puts("\tWhen a variable is put in register the access time is reduced. Generally compilers do these neccesary optimizations");
  puts("\tNevertheless, it's choice of the compiler to put the variable in register memory.");
}


void privatefunction(){
  puts("[main] Printing from private function");
}

// static int add_2(int,int);
int add_2(int,int);

void externproperties() {
  puts("\n");
  OS_VERSION = "Linux";
  printf("external bmain: %d\n", bmain);
  int bmain = -1;
  printf("local no-external bmain: %d\n", bmain);
  // here extern cnormal will take the (global) variable defined outside the
  // scope
  extern int cnormal;
  printf("cnormal: %d\n", cnormal);

  printExternalVars();
  privatefunction();

  printf("add_2(int,int) = %d\n",add_2(8, 22));
}

void staticvar() {
  // Initializer element is not a compile-time constant:
  // int preval = 24;
  // static int val = preval;
  puts("Static vars as well as const global variables, global and extern that are not initialized will be stored in data segment bss");
  static int val = 23;
  static int valunitialized;
  val++;
  printf("\tstatic variable 'val' has: %d\n", val);
  printf("\tstatic Unitialized variable 'valunitialized' has: %d\n", valunitialized);
}

int varunint;
void autovars() {
  puts("auto variables don't waste memory since they are automatically "
       "destroyed");
  int var;       // this variable is auto by default
  auto int var2; // so it's not neccesary to add the keyword 'auto'
  puts("If a variable is not initialized it will print a 'Garbage value'");
  printf("Garbage value: %d\n", var);
  printf("Unitialized global varieable value: %d (by default they are "
         "initialized to 0)\n",
         varunint);
}
void foo(void) { int foo_var = 42; }

void bar(void) {
  int bar_var;
  printf("%d\n", bar_var);
}

int x2 = 100;
int main(void) {
  // garbage values:
  // bar();
  // foo();
  // bar();
  // its garbage value because the right assigment does not take the global variable x2 = 100;
  int x2 = x2;
  int localx = localx;
  printf("[garbage value] x2,localx is %d,%d\n",x2,localx);
  // end garbage values
  unsigned bvar = 10;
  // when you negate the bits of a number in this representation you get that number in negative minus one
  printf("bvar and ~bvar: %d,%d\n",bvar,~bvar);

  puts("====================Running program==========================");

  puts("1) printf functions returns the number of characters that it "
       "successfully prints on the screen");
  puts("\tso you can use something like: int v = printf(\"%s\",\"Hi!\");");
  printf("successfully printed chars of \"Hi!\\n\": %d\n",
         printf("%s\n", "Hi!"));

  char c = 255;
  puts("\n\nchar c = 255 + 10 will print in number:");
  printf("\t%d which is equals to 265 mod 256\n", c + 10);

  puts("\n\nUsing signed varname or unsigned varname is valid, since the "
       "compiler automatically assigns the 'int' type by default ");
  signed iv;
  unsigned uiv;

  puts("\n\nunsigned i = 1 + int j = -4:");
  unsigned i = 1;
  int j = -4;
  printf("Using %%u: %u\n", i + j);
  printf("Using %%d: %d\n", i + j);

  puts("\n\nUsing static variables inside functions");
  staticvar();
  staticvar();
  // here 'val' cannot be accesed:
  // printf("\tstatic variable 'val' has: %d\n",val);

  puts("\n");
  autovars();
  externproperties();
  registers();
  macros();
  commaoperator();
  precedence();
}
