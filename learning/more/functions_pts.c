#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void __attribute__((constructor)) preventSTDIN() {

  if (!isatty(STDIN_FILENO)) {
    printf("Input is being redirected from a file.\n");
    exit(2);
  }
};

double add(double a, double b) { return a + b; }

double sub(double a, double b) { return a - b; }

double prod(double a, double b) { return a * b; }

// (type,type), (type), (...) etc represents a function
// so (*pfn)(type...) represents a pointer to a function
// double operation( double (*fn)(double,double), double a, double b){
double operation(double(fn)(double, double), double a, double b) {
  return fn(a, b);
}

enum Op {
  ADD = '+',
  SUB = '-',
};

//@BEST_PRACTICE
typedef int (*math_fnc)(int, int);

/* add: add a and b, return result */
int add2(int a, int b) { return a + b; }
/* sub: subtract b from a, return result */
int sub2(int a, int b) { return a - b; }
/* getmath: return the appropriate math function */
int getmath(math_fnc mathfn) { return mathfn(78, 10); }

int main(void) {

  printf("add2: %d\nsub2: %d\n", getmath(add2), getmath(sub2));

  return 0;

  // here is needed the * in arratFn
  double (*arrayFn[3])(double, double) = {add, sub, prod};

  double x = 24, y = 2.8, result, result2;
  puts("\n\nPointers to functions:\n");
  printf("\tadd(%.2f,%.2f) = %.2f\n", x, y, operation(&add, x, y));
  printf("\tsub(%.2f,%.2f) = %.2f\n", x, y, operation(&sub, x, y));
  double (*fn2)(double, double) = prod;
  double (*fn3)(double, double) = &prod;
  result = fn2(x, y);
  result2 = fn3(x, y);
  printf("\t[non *]prod(%.2f,%.2f) = %.2f\n", x, y, result);
  printf("\t[*]prod(%.2f,%.2f) = %.2f\n", x, y, result2);

  int opc;
  puts("\n\nOperations Menu:");
  puts("\t1) Sum 2 numbers");
  puts("\t2) Substract 2 numbers");
  puts("\t3) Multiply 2 numbers");
  printf("\nType the option followed by two numbers> ");
  scanf("%d %lf %lf", &opc, &x, &y);
  result = arrayFn[opc - 1](x, y);
  printf("The result is: %.2lf\n", result);

  return 0;
}
