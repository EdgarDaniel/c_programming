#include <stdio.h>
#include <string.h>

void copying() {
  puts("\n\nUsing strcpy():");
  puts("Copying char * c = \"Hello\" to char c[10] and char * c3 = \"Hi\";");

  char *c = "Hello";
  char c2[10] = "World";
  //
  char *c3 = "Hi";

  printf("\tc2 before copying: %s\n", c2);
  // strcpy returns a pointer to the first char of c2, so can use
  // strcpy(c2,c)[0] to get only the first char
  printf("Copying: %s\n", strcpy(c2, c));
  puts("copying to * c3 cause undefined behavior/crash");
  printf("\tc2 after copying: %s\n", c2);
  printf("\tc3 after copying: %s\n", c3);

  puts("\ncopying with strncpy():");
  puts("Copying char * c4 = \"Hello there, how are you?\" to char c5[15]");
  char *c4 = "Hello there, how are you?";
  char c5[15];
  // printf("c4 has a size of: %zu and length of %zu\n",sizeof(c4),strlen(c4));
  printf("Copying: %s\n", strncpy(c5, c4, sizeof(c5) - 1));
  c5[sizeof(c5) - 1] = '\0';
  printf("\tc5 after copying: %s\n", c5);
}

void lengthString() {
  puts("\n\nLength of strings");
  char *c = "Hello";
  char c2[] = "Hi!";
  char c3[10] = "Hi you!";
  char c4[15] = "Good\0Bad";

  printf("\t%s len is %zu\n", c, strlen(c));
  printf("\t%s len is %zu\n", c2, strlen(c2));
  printf("\t%s len is %zu and sizeof %zu\n", c3, strlen(c3), sizeof(c3));
  printf("\t%s (Good\\0Bad) len is %zu\n", c4, strlen(c4));
  printf("\t(Good\\0Bad)[5]: %c\n", c4[5]);
}

void concatination() {
  puts("\n\nConcat strings");
  char *world = "World!";
  char message[50] = "Hello";
  printf("\tmessage before concatination of %s = %s\n", world, message);
  strcat(strcat(message, " "), world);
  printf("\tmessage after concatination = %s\n", message);

  puts("\n\nNconcat strings with strncat()");
  char *linuxDist = "Linux: OpenSuse Tw";
  char message2[12] = "Using ";
  printf("\tConcating %s to message2 of fixed size 12 and length %zu: %s\n",
         linuxDist, strlen(message2), message2);
  strncat(message2, linuxDist, sizeof(message2) - strlen(message2) - 1);
  printf("\tAfter concating: %s (lenth: %zu and sizeof: %zu)\n", message2,
         strlen(message2), sizeof(message2));
}

void printComparision(char * str1, char * str2){

  printf("\tComparing %s and %s\n", str1, str2);
  int res = strcmp(str1, str2);
  if (res < 0) {
    printf("\t\t%s is lesser than %s\n", str1,str2);
  } else if (res > 0) {
    printf("\t\t%s is greater than %s\n", str1, str2);
  } else {
    printf("\t\t%s is equal to %s\n", str1, str2);
  }
}


void printNComparision(char * str1, char * str2, int n){

  printf("\tComparing first %d bytes of %s and %s\n", n,str1, str2);
  int res = strncmp(str1, str2,n);
  if (res < 0) {
    printf("\t\t%s is lesser than %s\n", str1,str2);
  } else if (res > 0) {
    printf("\t\t%s is greater than %s\n", str1, str2);
  } else {
    printf("\t\t%s is equal to %s\n", str1, str2);
  }

}

void comparition() {
  puts("\n\nComparing strings");
  char *date1 = "2023-03-01";
  char *date2 = "2023-02-28";

  printComparision(date1, date2);
  printComparision(date2, date1);
  printComparision("e", "é");
  printComparision("A", "A");
  printComparision("A", "a");
  printComparision("abcd", "abce");
  
  printNComparision("abcd", "abce",3);
}

void question1(){
  puts("\n\nQuestion 1");
  char p[20];
  char * s = "string";
  size_t length = strlen(s);
  size_t i=0;

  for(; i<length; i++){
    p[i] = s[length - i];
  }
  printf("%s\n",p);
  printf("p[1] = %c\n",p[1]);
  printf("p[2] = %c\n",p[2]);
  printf("p[3] = %c\n",p[3]);
  printf("p[4] = %c\n",p[4]);
  printf("p[5] = %c\n",p[5]);
}

void fun1(char *s1, char * s2){
  char *tmp;
  tmp = s1;
  s1 = s2;
  s2 = tmp;
}

void fun2(char **s1, char **s2){
  char *tmp;
  tmp = *s1;
  *s1 = *s2;
  *s2 = tmp;
}


void fun3(char *s1, char * s2){
  char **tmp;
  tmp = &s1;
  *s1 = *s2;
  // *s2 = *tmp;
}

void question2(){
  puts("\n\nQuestion 2");
  char * str1 = "Hi", * str2 = "Bye";
  fun1(str1,str2);
  printf("%s %s\n",str1,str2);
  fun2(&str1,&str2);
  printf("%s %s\n",str1,str2);
  // fun3(str1,str2);
  printf("%s %s\n",str1,str2);

}

void question3(){
  puts("\n\nQuestion 3");
  char * c = "GATECSIT2017";
  printf("*c = \"%s\"\n",c);
  char *p = c;
  puts("\tGiven *p is a pointer then: 2[p] = *(2+p) and 6[p] = *(6+p)");
  printf("\t%s with length %d\n", p+2[p]-6[p]-1 ,(int) strlen(p+2[p]-6[p]-1) );
}

void subswithpointer(){

  char c[] = "GATE2011";
  char *p = c;
// assuming p pointer is 1000 then:
  // printf("%s\n",p+p[4]-p[1]);
  // would be
// printf("%s\n",1000 + E - A) = 1000 + 69-65 = 1004
  // then will print from adress 1004 (since p is in 1000 then 1004 points '2')
  printf("%s\n",p);
  p+=2;
  printf("%s\n",p);
  p+=2;
  printf("%s\n",p);
}

int main(void) {
  copying();
  lengthString();
  concatination();
  comparition();
  question1();
  subswithpointer();
  question2();
  question3();
  return 0;
}
