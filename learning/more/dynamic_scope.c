#include <stdio.h>

int fun1(int);
int fun2(int);

int a=90;

int main(void)
{
  int a = 10;
  a = fun1(a);
  printf("%d\n",a);
  return 0;
}

int fun1(int b){
  b += 10;
  b = fun2(b);
  return b;
}

int fun2(int b){
  int c;
  // this shall take the a value from main() in dynamic scoping 
  // C, algol, pascal, etc are statically scoped
  //  SNOBOL, Lisp, APL, etc are dinamically scoped
  //  Perl support both static and dynamic scoping
  c = a + b;
  return c;
}
