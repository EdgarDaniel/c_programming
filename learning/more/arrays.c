#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// these do not work
// #pragma startup func1
// #pragma exit func2
#define print_any(X)                                                           \
  _Generic((X),                                                                \
      int: print_int,                                                          \
      default: print_unknown,                                                  \
      char: print_char,                                                        \
      float: print_float)(X)

// #define GEN_PRINT(TYPE, SPECIFIER_STR) int print_##TYPE(TYPE x) { return
// printf(SPECIFIER_STR "\n", x);} GEN_PRINT(int, "%d"); GEN_PRINT(char, "%c");
// GEN_PRINT(float, "%f");

int print_int(int i) { return printf("%d", i); }

int print_char(char i) { return printf("%c", i); }

int print_float(float f) { return printf("%f", f); }

int print_unknown(char c) { return printf("ERROR: Unknown type\n"); }

void __attribute__((constructor)) func1();
int __attribute__((destructor)) funcExit();

// With this, all identifires are blocked
#pragma GCC poison barray


void swapNums(int *v1, int *v2){
  int tmpVal = *v1;
  *v1 = *v2;
  *v2 = tmpVal;
}

void swapping(){
  int n=10,m=3;
  printf("%d,%d\n",n,m);
  swapNums(&n, &m);
  printf("%d,%d\n",n,m);
}

void func1() {
  printf("\n=================Run before main=====================\n");
}

int funcExit() {
  printf("bye! (run after main)\n");
  // exit(2);
  puts("exiting...");
  return 2;
}

void sum5ToArray(int array[], size_t size) {
  for (size_t i = 0; i < size; i++) {
    array[i] += 5;
  }
}
// You cannot do it that way. When you pass an array to a function, it decays
// into a pointer to the first element, at which point knowledge of its size is
// lost.
void printArray(int array[], char *varType, size_t size) {
  puts("\n===============================================================\n");
  char str[30] = "array[%zu] = ";
  strcat(str, varType);
  for (size_t i = 0; i < size; i++) {
    printf(str, i, array[i]);
  }
  puts("\n===============================================================\n");
}

void printPArray(int *array, size_t size) {
  puts("\n===============================================================\n");
  for (size_t i = 0; i < size; i++, array++) {
    printf("array[%zu] = %d\n", i, *array);
  }
  puts("\n===============================================================\n");
}

void printCharBArray(size_t size_i, size_t size_j, char array[][size_j]) {
  for (size_t i = 0; i < size_i; i++) {
    for (size_t j = 0; j < size_j; j++) {
      printf("| %c ", array[i][j]);
    }
    puts("|");
  }
  puts("\n");
}

void printIntBArray(size_t size_i, size_t size_j, int array[][size_j]) {
  for (size_t i = 0; i < size_i; i++) {
    for (size_t j = 0; j < size_j; j++) {
      printf("| %d ", array[i][j]);
    }
    puts("|");
  }
  puts("\n");
}

// C stores multi-arrays in row major order and therefore pointer++ will iterate
// row by row printing each value of each column of the current row
void printCharPtBArray(size_t size_i, size_t size_j, char *array) {
  size_t total = size_i * size_j;
  for (size_t i = 0; i < total; i++) {
    // printf("| %c ",*array);
    printf("| ");
    print_any(*array);
    printf(" ");
    if ((i + 1) % size_j == 0)
      puts("|");
    array++;
  }
  puts("\n");
}

void printPtIntColumnNArray(int *array, int column, size_t size_i,
                            size_t size_j) {
  // Como solo se requiere imprimir la columna N entonces el iterador seria
  // igual i=0, i<size_i
  array += column;
  puts("\n");
  for (size_t i = 0; i < size_i; i++) {
    // el puntero array se incrmenta tomando en cuenta el numero de columnas:
    // size_j:
    printf("array[%zu][%zu] = ", i, 0ul);
    print_any(*array);
    puts("");
    array += size_j;
  }
  puts("\n");
}

void readIntBArray(size_t rows, size_t columns, int array[rows][columns]) {
  puts("\nAssigning values:");
  for (size_t i = 0; i < rows; i++) {
    for (size_t j = 0; j < columns; j++) {
      printf("\tType value for array[%zu][%zu]: ", i, j);
      scanf(" %d", &array[i][j]);
    }
  }
}

#define N 5
#define M 2

void bidiarrays() {
  // char barray[3][3] = {'X'};
  char bidarray[][3] = {
      {'X'},
      {'X'},
      {'X'},
  };
  char bidarray2[3][3] = {'A', 'C', 'I', 'L', 'O', 'X', {'P', 'Q', 'R'}};
  // char  ** pbidarr = bidarray2; // no valid
  // Using:
  // int array[N][M] the variable array (or making char * parr = array) will
  // point to the first Unidimensional array, means: *parr points to array[0]
  // which will point to array[0][0]
  printCharBArray(3, 3, bidarray);
  printCharPtBArray(3, 3, &bidarray2[0][0]);

  puts("Printing bidarray2 using *(*(bidarray2)+index)");
  for (int i = 0; i < 9; i++) {
    printf("Current item from bidarray2: %c\n", *(*(bidarray2) + i));
  }

  puts("\nPrinting bidarray2 using *(*(bidarray2+index_i)+index_j)");
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      printf("Current item from bidarray2[%d][%d](%p): %c\n",i,j, &bidarray2[i][j] , *(*(bidarray2+i) + j));
    }
  }

  // printf("%c\n",**bidarray2);
  // printf("%c\n",*(*(bidarray2)+1));
  // printf("%c\n",*(*(bidarray2)+2));
  //
  // printf("%c\n",*(*(bidarray2+1)));
  // printf("%c\n",*(*(bidarray2+1)+1));

  int bidintarray[N][M] = {
      {23, 26}, {100, 101}, {-5, 12}, {999, 1001}, {2023, 0}};

  printPtIntColumnNArray(bidintarray[0], 1, N, M);

  int rows, columns;

  printf("Give rows: ");
  scanf(" %d", &rows);
  printf("Give columns: ");
  scanf(" %d", &columns);

  // C99 standard (and seems to be only for gcc and clang) can intialize array
  // by using user input this is not allowed: int buserArray[rows][columns] =
  // {{0}}; this should be used: this is called: VLA: variable length array
  // static int buserArray[rows][columns]; // error: Variable length array
  // declaration cannot have 'static' storage duration

  int buserArray[rows][columns];
  memset(buserArray, 0, rows * columns * sizeof(int));
  printIntBArray(rows, columns, buserArray);
  readIntBArray(rows, columns, buserArray);
  printIntBArray(rows, columns, buserArray);
}

void printStringUniArray(size_t size, char *strUniArray[]) {
  puts("\nUnidimensional string array:");
  printf("Sizeof array: %zu\n", size);
  for (size_t i = 0; i < size; i++) {
    printf("array[%zu] = %s\n", i, strUniArray[i]);
  }
}

void printStringPointUniArray(size_t size, char *strUniArray[]) {
  puts("\nUnidimensional string array (using pointers):");
  printf("Sizeof array: %zu\n", size);
  for (size_t i = 0; i < size; i++) {
    // printf("array[%zu] = %s\n",i,*strUniArray);
    // strUniArray++;
    // Or:
    // printf("array[%zu] = %s\n",i,*(strUniArray++));
    printf("array[%zu] = %s\n", i, *strUniArray++);
  }
}

void printRevStringPointUniArray(size_t size, char *strUniArray[]) {
  puts("\nUnidimensional reverse string array (using pointers):");
  printf("Sizeof array: %zu\n", size);
  for (char **i = &strUniArray[size - 1]; i >= &strUniArray[0]; i--) {
    printf("curret value = %s\n", *i);
  }
}

void printStringPointBidiArray(size_t size_i, size_t size_j,
                               char *array[][size_j]) {

  char **strUniArray = array[0];
  size_t size = size_j * size_i;
  puts("\nBidimensional string array (using pointers):");
  printf("Sizeof array: %zu\n\n", size);
  // *strUniArray="init";
  for (size_t i = 0; i < size; i++) {
    printf("| %7s ", *strUniArray);
    strUniArray++;
    // this access to env variables:
    // strUniArray++;
    // and the parameter shoyld be char * array[][size_j]
    if ((i + 1) % size_j == 0) {
      // == 0 then:
      puts("\n");
    }
  }
}

void stringarrays() {
  puts("\n\n");
  char *strArray[] = {
      "OpenSuse", "RedHat", "Debian", "Ubuntu", "Arch Linux", "Fedora",
  };

  size_t size = sizeof(strArray) / sizeof(strArray[0]);

  printStringUniArray(size, strArray);
  printStringPointUniArray(size, strArray);
  printRevStringPointUniArray(size, strArray);
  puts("\n\n");

  char *strBidArray[2][5] = {{"Linux", "Suse", "RedHat", "Debian", "Alpine"},
                             {"*BSD", "MacOS", "OpenBSD", "FreeBSD", "NetBSD"}};
  printStringPointBidiArray(2, 5, strBidArray);
  puts("\n\n");
}

int main(void) {
  int x[] = {10, 103, 45, 45, 49, 1};
  int y[5] = {1, 2, 3};
  // Note: This should not be used
  int z[] = {};
  int a[] = {[0] = -1, [8] = -100, [10] = -30};

  size_t size = sizeof(x) / sizeof(x[0]);
  printArray(x, "%d\n", size);
  sum5ToArray(x, size);
  printArray(x, "%d\n", size);

  puts("Printing y array");
  size = sizeof(y) / sizeof(y[0]);
  int *yp = y;
  yp[0] = 10;
  yp[1] = 200;
  yp[2] = 3000;
  printPArray(y, size);
  int (*ppy)[5] = &y;
  printf("*(*ppy) = %d\n",*(*ppy));
  printf("*(*ppy+1) = %d\n",*(*ppy+1));
  printf("*(*ppy+2) = %d\n",*(*ppy+2));

  printf("(*ppy)[0] = %d\n",(*ppy)[0]);
  printf("(*ppy)[1] = %d\n",(*ppy)[1]);
  printf("(*ppy)[2] = %d\n",(*ppy)[2]);

  puts("\nPrinting z array");

  size = sizeof(z) / sizeof(z[0]);
  printPArray(z, size);

  size = sizeof(a) / sizeof(a[0]);
  printPArray(a, size);

  stringarrays();
  bidiarrays();

  // this is valid
  int vv = 3;
  int *pvv = &vv;
  pvv[0] = 1;
  // pvv[1] = 10;
  printf("vv has %d\n", vv);
  swapping();

  return 0;
}
