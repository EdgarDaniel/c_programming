#include "vars.h"
#include <stdio.h>

int bmain = 150;

void privatefunction(){
  puts("[outside] Printing from private function");
}

static int add_2(int a, int b){
// int add_2(int a, int b){
  return a+b;
}

void printExternalVars(){
  printf("[outside] OS_VERSION is %s\n",OS_VERSION);
  printf("[outside] external bmain: %d\n", bmain);
}
