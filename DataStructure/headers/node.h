#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

#include <stdio.h> 

struct NodeList{
	void *data;
	struct NodeList *next;
};

void add_begin(void *data);
void add_end(void *data);
void show_list();
void show_first();
void show_last();

#endif // NODE_H_INCLUDED
