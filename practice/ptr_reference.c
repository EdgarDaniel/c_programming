#include <stdio.h>
#include <stdlib.h>

struct Point  {
  int x;
  int y;
  struct Point *point;
};

void resetPoint(struct Point ** point){
  (*point)->x = -1;
  (*point)->y = -1;


  (*point)->point = malloc(sizeof(**point));
  (*point)->point->x = 2;
  (*point)->point->y = 44;
}


void resetPoint2(struct Point * point){
  point->x = -12;
  point->y = -14;


  point = malloc(sizeof(*point));
  point->x = 12;
  point->y = 42;
}

struct Point * point;

int main(int argc, char *argv[])
{
  point = malloc(sizeof(*point));

  point->x = 120;
  point->y = 13;

  printf("X,Y = %d,%d\n",point->x,point->y);
  // resetPoint(&point);
  // printf("X,Y = %d,%d\n",point->x,point->y);
  // printf(".X,.Y = %d,%d\n",point->point->x,point->point->y);
  resetPoint2(point);
  printf("X,Y = %d,%d\n",point->x,point->y);
  printf(".X,.Y = %d,%d\n",point->point->x,point->point->y);

  return EXIT_SUCCESS;
}
