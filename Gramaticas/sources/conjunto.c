#include "../headers/conjunto.h"
#include <stdio.h>
#include <string.h>
#include "../headers/stack.h"
#include <malloc.h>

#define LEN(arr) ((int) (sizeof (arr) / sizeof (arr)[0]))
#define ITERATOR(begin,size) for(int ind=begin; ind<size; ind++)
#define ITERATOR_BOOL(begin,get) for(int i=begin; get; i++)

char *N[] = {"S","Lv","Var"};
char *T[] = {"x1","x2","x3","x4","x5", ":", ",", "Int"};
char *P[][8] = {
		{""},
		{"","Lv : Int","\0"},	
		{"","Lv , Var","Var","\0"},
		{"","x1","x2","x3","x4","x5","\0"}
};
int nro_ntls = LEN(N)+1;
char firstPG[]="S";



void init(){
	
	P[0][0] = strcat(firstPG,"'");
	P[0][1] = "S";
	P[0][2] = "\0";
	ITERATOR(0,nro_ntls){
		P[ind+1][0] = N[ind];
	}
}

char *FirstOf[LEN(N)+1][50]={
		{},
		{},
		{},
		{},
};

char* get_firstOf(char *str, unsigned long size){
	
 	
	int count=0;
	
	ITERATOR_BOOL(0, str[i]!=' ' && i<size){
		count++;
	}

	char *f = malloc (sizeof (char));
	ITERATOR(0,count){
		f[ind] = str[ind];
	}
	
	return f;
}

int isnoterminal(char *car)
{
	
	ITERATOR(0,nro_ntls)
	{
		if(!strcmp(car,P[ind][0]))
			return 1;
	}
	
	return 0;
}

void addF_ifnotexists(char *string, int row,int *col){
	
	
	ITERATOR(0,*col)
	{
		if(!strcmp(FirstOf[row][ind],string))
			return;
	}
	
	FirstOf[row][(*col)++] = string;
}

char ** get_firstOfNt(char *no_terminal)
{
	char ** array = malloc(sizeof(char));

	int index=-1;
	
	ITERATOR(0,nro_ntls){
		
		if(!strcmp(FirstOf[ind][0],no_terminal)){
			index=ind;
			break;
		}
	}	
	
// 	printf("F(%s) = { ",no_terminal);
	if(index!=-1)
 	{
		ITERATOR_BOOL(1,FirstOf[index][i]!="\0"){
// 			printf(" [%s] ",FirstOf[index][i]);
			array[i-1]=FirstOf[index][i];
		}
// 		puts("}");
	}

	
	return array;
}

void findFirsts()
{
	char *fr;
	int actIndex;
	ITERATOR(0,nro_ntls){
		FirstOf[ind][0] = P[ind][0];
		
		actIndex=1;
		
		ITERATOR_BOOL(1, P[ind][i]!="\0"){
			
			fr = get_firstOf(P[ind][i],strlen(P[ind][i]));
			
			if(isnoterminal(fr)){
				FirstOf[ind][actIndex++]=fr;
			}else{

				addF_ifnotexists(fr,ind,&actIndex);
			}
		}
		FirstOf[ind][actIndex++]="\0";
		puts("\n");
	}
	
	ITERATOR(0,nro_ntls)
	{
		printf("F(%s) = { ",FirstOf[ind][0]);
		ITERATOR_BOOL(1,FirstOf[ind][i]!="\0"){
			printf(" [%s] ",FirstOf[ind][i]);
		}
				
		puts("}");
	}
// 	for(int i=nro_ntls-1; i>=0; i--)
// 	{
// 		
// 	}
	
// 	printf("Var: %s\n",get_firstOfNt("Var")[5]);
}



char * derivateNt(char * nt){
	char *array = malloc(sizeof(char));
	int x=10;
	do{
		
		int y=x+20;
		char x='D';
		
		
	}while(1);
	
	const int  * x = $var1,  * y,  * m = $var2;
	int $iVar;
	$iVar = $var2 + $var2 ;
	y = (int *)$iVar;
		
	
	return array;
}



void printPrductions(){
	
	for(int i=0; i<nro_ntls; i++)
 	{
		printf("%s --> ",P[i][0]);
		
		for(int j=1; strcmp(P[i][j],"\0");j++) 
  		{
			printf(" %s |",P[i][j]);
		}
		puts("\n");
		
	}
}

void printNoTerminals(){
	
	puts("\n-----No Terminals------\n");
	int size = sizeof(N) / sizeof(N[0]);
	printf("The size is: %d\n",size);
	
	for(int i=0; i<size; i++){
		printf("The %d element is %s\n",i,N[i]);
	}
}

void printTerminals(){
	
	puts("\n-----Terminals------\n");
	int size = sizeof(T) / sizeof(T[0]);
	printf("The size is: %d\n",size);
	
	for(int i=0; i<size; i++){
		printf("The %d element is %s\n",i,T[i]);
	}
}

void execute()
{
	init();
	printf("Cant: %d\n",nro_ntls);
// 	char cars[] = "x1";
// 	printf("Is not terminal %s? %d\n",cars,isnoterminal(cars));
// 	expandG();
// 	enumerarG();
	findFirsts();
}

void expandG(){
	
	puts("1) Expandir Gramatica");
	
	ITERATOR(1,nro_ntls){
		
		for(int j=1; strcmp(P[ind][j],"\0");j++) 
		{
			printf(" %s --> %s\n",P[ind][0],P[ind][j]);
		}
	}
	
	
	
}

void enumerarG(){
	puts("2-3) Enumerando");
	
	int cont=0;
	
	ITERATOR(0,nro_ntls)
	{
		for(int j=1; strcmp(P[ind][j],"\0");j++) 
		{
			
			printf("P%d) %s --> %s\n",cont,P[ind][0],P[ind][j]);
			cont++;
			
		}		
	}
}
