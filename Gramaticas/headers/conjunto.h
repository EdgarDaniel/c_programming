#ifndef CONJUNTO_H_INCLUDED
#define CONJUNTO_H_INCLUDED

extern char *N[];
extern char *T[];


void printNoTerminals();
void printTerminals();
void printPrductions();
void execute();
void expandG();
void enumerarG();
void findFirsts();

int isnoterminal(char *car);

#endif // CONJUNTO_H_INCLUDED
